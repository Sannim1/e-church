-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 02, 2014 at 11:18 AM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `echurch_church0`
--

-- --------------------------------------------------------

--
-- Table structure for table `devotions`
--

CREATE TABLE IF NOT EXISTS `devotions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_title` varchar(128) NOT NULL,
  `file_description` varchar(400) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `devotions`
--

INSERT INTO `devotions` (`id`, `file_title`, `file_description`, `file_name`) VALUES
(13, ' fsdf ', ' dfsdfsd ', ' ../devotions/pdo.pdf ');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(150) NOT NULL,
  `event_description` text NOT NULL,
  `event_location` varchar(150) NOT NULL,
  `date` varchar(15) NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`ID`, `event_name`, `event_description`, `event_location`, `date`, `time`) VALUES
(5, 'Code Camp', 'Last day of Code Camp', 'CC HUB', '2014-08-28T08:0', 1409212800),
(6, 'Code Camp', 'Last day of Code Camp', 'CC HUB', '2014-08-28T08:0', 1409212800),
(7, 'Code Camp', 'Last day of Code Camp', 'CC HUB', '2014-08-28T08:0', 1409212800),
(8, 'Code Camp', 'Last day of Code Camp', 'CC HUB', '2014-08-28T08:0', 1409212800),
(9, 'Code Camp', 'Last day of Code Camp', 'CC HUB', '2014-08-28T08:0', 1409212800),
(10, 'Code Camp', 'Last day of Code Camp', 'CC HUB', '2014-08-28T08:0', 1409212800),
(11, 'Code Camp', 'Last day of Code Camp', 'CC HUB', '2014-08-28T08:0', 1409212800);

-- --------------------------------------------------------

--
-- Table structure for table `gcm_ids`
--

CREATE TABLE IF NOT EXISTS `gcm_ids` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` text NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `gcm_ids`
--

INSERT INTO `gcm_ids` (`ID`, `client_id`, `time`) VALUES
(1, 'APA91bEIM3dhaKKcd3UZxX4nAsfmVHoXKn7_wAgL-sTHufTeSQjiJmgbUwOiiNDDVcZJbqDwV3xMnlyF2AQtt7SRN3RM9CPGWrBKrwmPWg-4p6IfmmZAiYtFGSFmkgW2AiBbX03i6tTVpoMQCrOnncr3crnx9mnSrQ', 1408624101);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `file_type` int(1) NOT NULL,
  `file_title` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_title` varchar(128) NOT NULL,
  `file_description` varchar(400) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `file_title`, `file_description`, `file_name`) VALUES
(69, ' zxczx ', ' cxzczxc ', ' ../newsletters/pdo.pdf ');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `message`, `time`) VALUES
(1, 'test', 1407589274),
(2, 'test', 1407592889),
(3, '', 1407593577),
(4, '', 1407593875),
(5, 'testing', 1407593895),
(6, 'testing', 1407594199),
(7, 'testing', 1407594208),
(8, 'testing', 1407594647),
(9, 'testing', 1407594770),
(10, 'testing', 1407614467),
(11, 'testing', 1407661559),
(12, 'testing', 1408624689),
(13, 'testing', 1408624726),
(14, 'testing', 1408625113),
(15, 'testing', 1408625132),
(16, 'testing', 1408625245),
(17, 'testing', 1408626072),
(18, 'testing', 1408626087),
(19, 'testing', 1408626143),
(20, 'testing', 1408626252),
(21, 'testing', 1408626463),
(22, 'testing', 1408626500),
(23, 'testing', 1408626623),
(24, 'testing', 1408626636),
(25, 'testing', 1408626848),
(26, 'testing', 1408626863),
(27, 'testing', 1408626912),
(28, 'testing', 1408627122),
(29, 'testing', 1408627172),
(30, 'testing', 1408627172),
(31, 'testing', 1408627243),
(32, 'testing', 1408627279),
(33, 'testing', 1408627283),
(34, 'testing', 1408627284),
(35, 'testing', 1408627315),
(36, 'testing', 1408627437),
(37, 'testing', 1408627461),
(38, 'testing', 1408627470),
(39, 'testing', 1408627527),
(40, 'testing', 1408627562),
(41, 'testing', 1408627624),
(42, 'testing', 1408627666),
(43, 'testing', 1408627717),
(44, 'testing', 1408627751),
(45, 'testing', 1408627771),
(46, 'testing', 1408627787),
(47, 'testing', 1408627942),
(48, 'testing', 1408628664),
(49, 'testing', 1408629409),
(50, 'This is how to hack', 1408629719),
(51, 'Still%testing', 1408630142),
(52, 'Testing Testing Testing', 1408635094);

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `church_id` int(5) NOT NULL,
  `status` int(1) NOT NULL,
  `um_feat` int(1) NOT NULL,
  `bs_feat` int(1) NOT NULL,
  `dnl_feat` int(1) NOT NULL,
  `s_feat` int(1) NOT NULL,
  `mm_feat` int(1) NOT NULL,
  `sp_feat` int(1) NOT NULL,
  `og_feat` int(1) NOT NULL,
  `mp_feat` int(1) NOT NULL,
  `em_feat` int(1) NOT NULL,
  `cp_feat` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE IF NOT EXISTS `signup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `church_name` varchar(128) NOT NULL,
  `church_location` varchar(128) NOT NULL,
  `pastor_name` varchar(128) NOT NULL,
  `pastor_number` varchar(128) NOT NULL,
  `pastor_email` varchar(128) NOT NULL,
  `church_email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `vision` varchar(500) NOT NULL,
  `mission` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `um_feat` int(1) NOT NULL,
  `bs_feat` int(1) NOT NULL,
  `dnl_feat` int(1) NOT NULL,
  `s_feat` int(1) NOT NULL,
  `mm_feat` int(1) NOT NULL,
  `sp_feat` int(1) NOT NULL,
  `og_feat` int(1) NOT NULL,
  `mp_feat` int(1) NOT NULL,
  `em_feat` int(1) NOT NULL,
  `cp_feat` int(1) NOT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`id`, `church_name`, `church_location`, `pastor_name`, `pastor_number`, `pastor_email`, `church_email`, `password`, `vision`, `mission`, `description`, `um_feat`, `bs_feat`, `dnl_feat`, `s_feat`, `mm_feat`, `sp_feat`, `og_feat`, `mp_feat`, `em_feat`, `cp_feat`, `status`) VALUES
(1, 'Foursquare', '7.517722099999999 , 4.5263479999999845', 'Skelewu', '080419911419', 'theif@ole.com', 'testing@echurch.com', 'password', 'Testing', 'Testing', 'Testing still Testing', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, b'0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
