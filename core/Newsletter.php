
<?php
require_once("data_access/db_connection.php");
require_once("data_access/CleanString.php");
require_once("data_access/DataTable.php");

class Newsletter{
    private $id;
    private $file_title;
    private $file_description;
    private $file_name;

    public function setFileDescription($file_description)
    {
        $this->file_description = $file_description;
    }

    public function getFileDescription()
    {
        return $this->file_description;
    }

    public function setFileName($file_name)
    {
        $this->file_name = $file_name;
    }

    public function getFileName()
    {
        return $this->file_name;
    }

    public function setFileTitle($file_title)
    {
        $this->file_title = $file_title;
    }

    public function getFileTitle()
    {
        return $this->file_title;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getResource(){
        $sql = "SELECT * FROM".Newsletters::table_name;
        $resource = mysql_query($sql);
        return $resource;
    }

    public static function fetchAll($resource){

        return mysql_fetch_assoc($resource);
    }

}