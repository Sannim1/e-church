<?php
  class GCM extends Base {
    public static function getGCM_IDS() {
      $gcm_obj = new GCM();

      $data = array();
      $statement = AppUserSqlStatement::GET_GCM_IDS;

      $result = $gcm_obj->executeQuery($statement, $data);
      $gcm_ids = array();
      foreach ($result as $row) {
        array_push($gcm_ids, $row[AppUserTable::gcm_id]);
      }
      return $gcm_ids;
    }

    public static function sendGoogleCloudMessage($data, $gcm_ids) {
      //------------------------------
      // Replace with real GCM API
      // key from Google APIs Console
      //
      // https://code.google.com/apis/console/
      //------------------------------

      //$apiKey = 'AIzaSyA5LoIrhz-ViAfP0IOQ51pA7cquyCaT06A';
      $apiKey = GCM_API_KEY;

      //------------------------------
      // Define URL to GCM endpoint
      //------------------------------

      //$url = 'https://android.googleapis.com/gcm/send';
      $url = GCM_URL;

      //------------------------------
      // Set GCM post variables
      // (Device IDs and push payload)
      //------------------------------

      $post = array(
        'registration_ids'  => $gcm_ids,
        'data'              => $data
      );

      //------------------------------
      // Set CURL request headers
      // (Authentication and type)
      //------------------------------

      $headers = array(
      'Authorization: key=' . $apiKey,
      'Content-Type: application/json'
      );

      //------------------------------
      // Initialize curl handle
      //------------------------------

      $ch = curl_init();

      //------------------------------
      // Set URL to GCM endpoint
      //------------------------------

      curl_setopt( $ch, CURLOPT_URL, $url );

      //------------------------------
      // Set request method to POST
      //------------------------------

      curl_setopt( $ch, CURLOPT_POST, true );

      //------------------------------
      // Set our custom headers
      //------------------------------

      curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

      //------------------------------
      // Get the response back as
      // string instead of printing it
      //------------------------------

      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

      //------------------------------
      // Set post data as JSON
      //------------------------------

      curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) );

      //------------------------------
      // Actually send the push!
      //------------------------------

      $result = curl_exec( $ch );

      //------------------------------
      // Error? Display it!
      //------------------------------

      if ( curl_errno( $ch ) )
      {
        //echo 'GCM error: ' . curl_error( $ch );
        return false;
      } else {
        return true;
      }

      //------------------------------
      // Close curl handle
      //------------------------------

      curl_close( $ch );
    }
  }
?>