<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 9/4/14
 * Time: 11:34 AM
 * To change this template use File | Settings | File Templates.
 */
require_once("query.php");

class Media{
    var $file_description;
    var $file_name;
    var $file_type;
    var $file_title;

    var $Query_Class;
    var $file_query;


    public function __construct($file_type){
        $this->file_type = $file_type;
      //  $this->Query_Class = new Query($this->setFileQuery($file_type));
          $this->Query_Class = new Query($this->setFileQuery($file_type));
    }

    public function setFileQuery($file_type)
    {

       $sql = "SELECT * FROM media WHERE file_type = ". $file_type;
        return $sql;
    }

    public function getFileQuery()
    {
        return $this->file_query;
    }


    public function setQueryClass($Query_Class)
    {
        $this->Query_Class = $Query_Class;
    }

    public function getQueryClass()
    {
        return $this->Query_Class;
    }

    public function setFileDescription($file_description)
    {
        $this->file_description = $file_description;
    }

    public function getFileDescription()
    {
        return $this->file_description;
    }

    public function setFileName($file_name)
    {
        $this->file_name = $file_name;
    }

    public function getFileName()
    {
        return $this->file_name;
    }

    public function setFileTitle($file_title)
    {
        $this->file_title = $file_title;
    }

    public function getFileTitle()
    {
        return $this->file_title;
    }

    public function setFileType($file_type)
    {
        $this->file_type = $file_type;
    }

    public function getFileType()
    {
        return $this->file_type;
    }

    public function getRecords(){
       return $this->Query_Class->getAll();
    }

    public static  function deleteRecord($id){
//        $path = '../'.'media/document/1409835472.pdf';
//        unlink($path);
        $sql = "DELETE FROM media WHERE id = ".$id;
        $delete_op =  new Query($sql);
       return $delete_op->do_query();

    }

}