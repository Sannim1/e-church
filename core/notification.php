<?php
  class Notification extends Base {
    private $message = NULL;
    private $title = NULL;
    private $time = NULL;

    public function __construct($data) {
      if (isset($data['message'], $data['title'])) {
        $this->title = $data['title'];
        $this->message = $data['message'];
      }
    }

    public function add() {
      if (($this->message !== NULL) && ($this->title !== NULL)) {
        $data = array();
        $data[NotificationsTable::title] = $this->title;
        $data[NotificationsTable::message] = $this->message;
        $data[NotificationsTable::time] = time();
//        die(var_dump($data));
        $stmt = NotificationSqlStatement::ADD;

        $result = $this->executeNonQuery($stmt, $data);

        return $result === -1 ? false : true;
      }

      return false;
    }
  }
?>