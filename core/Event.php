<?php
/**
 * Created by PhpStorm.
 * User: caleb
 * Date: 11/7/14
 * Time: 1:53 PM
 */

class Event extends Base {

    private $name = NULL;
    private $desc = NULL;
    private $location = NULL;
    private $date = NULL;
    private $time = NULL;

    public function __construct($data) {
        if (isset($data[EventsTable::name], $data[EventsTable::description], $data[EventsTable::location], $data[EventsTable::date], $data[EventsTable::time])) {

            $this->name = $data[EventsTable::name];
            $this->desc = $data[EventsTable::description];
            $this->location = $data[EventsTable::location];
            $this->date = $data[EventsTable::date];
            $this->time = $data[EventsTable::time];
        }
    }

    public function add(){
        if (($this->name !== NULL) && ($this->desc !== NULL) && ($this->location !== NULL) && ($this->date !== NULL) && ($this->time !== NULL)) {

            $data = array();
            $data[EventsTable::name] = $this->name;
            $data[EventsTable::description] = $this->desc;
            $data[EventsTable::location] = $this->location;
            $data[EventsTable::date] = $this->date;
            $data[EventsTable::time] = $this->time;

            $stmt = EventSqlStatement::ADD;

            $result = $this->executeNonQuery($stmt, $data);
            return ($result == -1) ? false : true;
        }
        return false;
    }
}