<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 8/7/14
 * Time: 6:51 PM
 * To change this template use File | Settings | File Templates.
 */
require_once("../data_access/CleanString.php");
require_once("../data_access/db_connection.php");
require_once("../data_access/DataTable.php");

class Upload{
    private $file_title;
    private $file_description;
    private $file_name;
    private $file_type;
    private $table_name;

    public function __construct($ft, $fd, $fn, $tn){
            $this->file_title = $ft;
            $this->file_description = $fd;
            $this->file_name = $fn;
            $this->table_name = $tn;
    }
    public function setFileDescription($file_description)
    {
        $this->file_description = $file_description;
    }

    public function getFileDescription()
    {
        return $this->file_description;
    }

    public function setFileName($file_name)
    {
        $this->file_name = $file_name;
    }

    public function getFileName()
    {
        return $this->file_name;
    }

    public function setFileTitle($file_title)
    {
        $this->file_title = $file_title;
    }

    public function getFileTitle()
    {
        return $this->file_title;
    }

    public function query($sql){
       return mysql_query($sql) or die(mysql_error().$sql);

    }

    public function setFileType($file_type)
    {
        $this->file_type = $file_type;
    }

    public function getFileType()
    {
        return $this->file_type;
    }

    public function sendToDb(){
        $sql = "INSERT INTO ".$this->table_name."(file_title, file_description, file_name, created_date, modified_date)
        VALUES( ' ".$this->file_title." ' , ' ".$this->file_description." ' , ' ".$this->file_name." ' , ' NOW() ' , ' NOW() ')";

        return  $this->query($sql);
    }

    public function sendToDbMedia(){
        $sql = "INSERT INTO ".$this->table_name."(file_title, description, file_name, file_type, created_date, modified_date)
        VALUES( '".$this->file_title."' , '".$this->file_description." ' , '".$this->file_name."', '". $this->file_type ."' , ' NOW() ' , ' NOW() ' )";

        return  $this->query($sql);
    }

}