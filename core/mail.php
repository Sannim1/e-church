<?php
/**
 * Created by PhpStorm.
 * User: caleb
 * Date: 8/6/14
 * Time: 12:15 PM
 */

class Mail{
    var $emailFromName;
    var $emailFromEmail;
    var $emailSubject;
    var $emailMessage;
    var $address;

    function __construct($name, $from, $subject, $message, $address){
        $this->emailFromName = $name;
        $this->emailFromEmail = $from;
        $this->emailSubject = $subject;
        $this->emailMessage = $message;
        $this->address = $address;
    }

    function send(){
        $header = 'From: ' . $this->emailFromEmail;
        $addition = '-f ' . $this->emailFromEmail;

        foreach($this->address as $to){
            $mail = mail( $to, $this->emailSubject, $this->emailMessage, $header, $addition);
            if($mail)
                return true;
            else
                return false;
        }
    }
}