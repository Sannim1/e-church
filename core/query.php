<?php
/**
 * Created by PhpStorm.
 * User: caleb
 * Date: 8/5/14
 * Time: 3:52 PM
 */
class Query{

    var $query;
    var $resultID;
    var $result;
    var $response;

    function __construct($query){
        $this->query = $query;
        $this->resultID = mysql_query($this->query);
    }

    function test(){
        echo HOST . DATABASE . "<br>";
        echo json_encode($this->query) . "<br>";
        echo json_encode($this->resultID) . "<br>";
    }

    public function do_query(){
        return $this->resultID;
    }
    function get(){
        if ($this->resultID){
            $this->result = mysql_fetch_array($this->resultID);
            if(!$this->result){
                $this->response = array("response" => "result error");
            }
            else{
                $this->response = array("response" => "success", "data" => $this->result);
            }
        }
        else{
            $this->response = array("response" => "query error");
        }
        return $this->response;
    }

    public function getAll(){
        if ($this->resultID){
            $this->response = array();
            for($i = 0; $i < mysql_num_rows($this->resultID); $i++){
                $this->result = mysql_fetch_assoc($this->resultID);
                if(!$this->result){
                    $this->response[$i] = "result error";
                }
                else{
                    $this->response[$i] = $this->result;
                }
            }
        }
        else{
            $this->response = array("response" => "query error");
        }
        return $this->response;
    }

    function status(){
        if ($this->resultID){
            return true;
        }
        else{
            return false;
        }
    }
}
