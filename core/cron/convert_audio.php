<?php
/**
 * Created by PhpStorm.
 * User: caleb
 * Date: 9/4/14
 * Time: 6:22 PM
 */
/*die(var_dump($_SERVER));*/
$s = explode('core', $_SERVER['SCRIPT_FILENAME']);
$data_access_path = $s[0] . 'data_access/';
/*die(var_dump($data_access_path));*/
require_once($data_access_path . 'Constants.php');

$files = glob(PATH_TO_AUDIO . '*.temp');

// if(is_executable('/var/www/html/echurch/core/cron/convert_audio.php')){
//    echo "cool";
// }
// else{
//    echo 'shing';
// }

foreach($files as $filename){
    if(file_exists($filename)){
        $name = $filename;
        $file = explode('.', $name);
        array_pop($file);
        $name = implode('.' , $file);

        rename($name . '.temp', $name . '.processing');
        sleep(1);

        $file = explode('/', $name);
        $noExt = end($file);

        $path = PATH_TO_AUDIO;

//        $command = 'ffmpeg -i ' .
//            $path  . $noExt . '.processing -strict -2 -ab 64k -ar 44100 -vcodec libx264 -s 640x480 '
//            . $path . $noExt . '.mp3';

        $command = '/usr/local/bin/ffmpeg -i ' .
            $path  . $noExt . '.processing '. $path . $noExt . '.mp3';

        echo $command;

        $output = shell_exec($command);

        unlink($name . '.processing');
    }
    else{
        continue;
    }
}