<!DOCTYPE HTML>
<?php
require_once("data_access/db_connection.php");
require_once("data_access/CleanString.php");
require_once("data_access/DataTable.php");
require_once("core/check_status.php");

require_once("core/Devotion.php");
require_once("core/query.php");

require("phase/info.php");

?>
<html>
<head>
    <title><?php echo $data['data']['church_name']; ?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" href="css/mystyle.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!--[if lte IE 8]>
    <script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.dropotron.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/skel-layers.min.js"></script>
    <script src="js/init.js"></script>
    <script src="js/myJs.js"></script>
    <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
    </noscript>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/ie/v8.css"/><![endif]-->
</head>
<body>

<!-- Header -->
<div id="header">

    <!-- Logo -->
    <h1><a href="profile.php" id="logo"><?php echo $data['data']['church_name']?> E-CHURCH <em>App</em></a></h1>

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="profile.php">Profile</a></li>
            <li><a href="notifications.php">Notifications</a></li>
            <li><a href="media.php" class="current">Media</a></li>
            <li><a href="event_scheduling.php">Events</a></li>
            <li class="current">
                <a href="">Inspirational</a>
                <ul>
                    <li><a href="newsletter.php">NewsLetter</a></li>
                    <li><a href="devotions.php">Devotions</a></li>
                </ul>
            </li>
            <li>
                <a class="signout" href="signout.php">Sign out</a>
            </li>
        </ul>
    </nav>
</div>

<section class="wrapper2 style1">
    <div class="upload_button">
        <input type="button" class="button alt" value="Upload" data-toggle="modal" data-target="#uploadModal" />
    </div>
</section>

<!-- Upload  modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form  name="uploadMedia" id="mForm" class="form-group" action="phase/phase_upload.php" method="post" onsubmit="return false">
            <div class="modal-content">
                <div id="response">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="signUpModalLabel">Upload</h4>
                    </div>
                    <div class="modal-body">
                        <div class="errorMsg" id="uploadError"></div>
                        <input class="form-control ejo" placeholder="Devotion Title" name="m_title">
                        <textarea class="form-control ejo" placeholder="Devotion Description" name="m_desc"></textarea>
                        <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-primary btn-file">
                                Browse&hellip; <input type="file" multiple name="f_name">
                            </span>
                        </span>
                            <input type="text" name="m_name" class="form-control" readonly>
                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" onclick="uploadDevotionForm()">Upload</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Main -->
<section class="wrapper style1">
    <div class="container">
        <div id="content">

            <!-- Content -->

            <article>
                <!--               -->
                <?php
                $sql = " SELECT * FROM ".Devotions::table_name;
                $resource = mysql_query($sql) or die("error in conn");
                if(mysql_num_rows($resource) == 0){
                    echo "<div class='emptyRecord'><p>Empty record</p></div>";
                }
                else{

                    while($row = mysql_fetch_assoc($resource)){
                        echo "<div class='records'>";
                        $id = $row[Devotions::id];
                        $file_name = $row[Devotions::file_name];

                        echo "<h3>".$row[Devotions::file_title]."</h3>";
                        echo "<p>".$row[Devotions::file_description]."</p>";
                        ?>
                        <a href='#' class='button small_button' onclick="deleteDevo(<?php echo $id?> , <?php echo "'".trim($file_name)."'"?>)">Delete</a>
                        <?php
                        echo "</div>";
                    }

                }

                ?>

            </article>

        </div>
    </div>
</section>

<!-- Footer -->
<div id="footer">


    <!-- Icons -->
    <ul class="icons">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa-github"><span class="label">GitHub</span></a></li>
        <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
    </ul>

    <!-- Copyright -->
    <div class="copyright">
        <ul class="menu">
            <li>&copy; eChurch All rights reserved</li>
        </ul>
    </div>

</div>
<script src="js/bootstrap.js"></script>
</body>
</html>