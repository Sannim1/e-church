<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 8/5/14
 * Time: 7:36 PM
 * To change this template use File | Settings | File Templates.
 */


function stringProof($input){
    if (get_magic_quotes_gpc()) $input = stripslashes($input);
    $input = htmlentities($input);
    $input =  strip_tags($input);
    return $input;
}

function cleanString($input){
    $input = mysql_real_escape_string($input);
    $input =  stringProof($input);
    return $input;
}