<?php
  class AppUserSqlStatement{
    const GET_GCM_IDS = 'SELECT DISTINCT(gcm_id) FROM app_user';
    const GET_EMAILS = 'SELECT DISTINCT(email) FROM app_user';
  }

  class DevotionsSqlStatement {
    const GET = "SELECT * FROM devotions";
  }

  class NewslettersSqlStatement {
    const GET = "SELECT * FROM newsletters";
  }

  class MediaSqlStatement{
    const  GET = "SELECT * FROM media WHERE file_type = :file_type";
  }

  class ProfileSqlStatement{
    const  GET = "SELECT * FROM signup";
  }

  class EventSqlStatement{
    const ADD = "INSERT INTO events (event_name, event_description, event_location, date, time, created_date, modified_date)
    VALUES (:event_name, :event_description, :event_location, :date, :time, NOW(), NOW())";
  }

  class NotificationSqlStatement {
    const ADD = 'INSERT INTO notifications(title, message, time, date, created_date, modified_date) VALUES(:title, :message, :time, now(), NOW(), NOW())';
  }
?>