<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 8/7/14
 * Time: 7:08 PM
 * To change this template use File | Settings | File Templates.
 */
require_once("../core/Upload.php");
require_once("../core/check_status.php");
require_once("../data_access/DataTable.php");




if(isset($_POST['m_title']) && isset($_POST['m_desc'])){
    $file_title = $_POST['m_title'];
    $file_description = $_POST['m_desc'];
    $file_type = $_POST['m_type'];
    //die(var_dump($_POST));
}
else{
    die("Incomplete fields");
}

switch($file_type){
    case 0:
        $path = "media/picture/";
        $limit = 2097152;
        $file = explode(".", $_FILES["f_name"]["name"]);
        $ext = trim(strtolower(end($file)));
        if(($ext == 'png') || ($ext == 'jpg') || ($ext == 'jpeg') || ($ext == 'gif') || ($ext == 'bmp')){
            $extension = $ext;
            break;
        }
        else{
            die('Wrong file type');
        }

    case 1:
        $path = "media/audio/";
        $limit = 104857600;
        $extension = 'mp3';
        break;
    case 2:
        $path = "media/video/";
        $limit = 524288000;
        $extension = 'mp4';
        break;
}

if(is_array($_FILES)) {
    $file_size = $_FILES['f_name']['size'];
    if($file_size > $limit){
        echo '<div class="output">';
        echo "<p>The file size is too large</p>";
        echo " <button type='button' class='btn btn-primary' data-dismiss='modal' onclick='reloadPage();'>Close</button>";
        echo "</div>";
    }
    else{
        if($file_type == 0)
            $ext = $extension;
        else
            $ext = 'temp';

        $rootName = time();
        $folderName = $rootName . "." . $ext;
        $dbName = $rootName . "." . $extension;

        $sourcePath = $_FILES['f_name']['tmp_name'];

        $targetPath = '../' . $path . $folderName;
//        $targetPathDB = 'http://' . $_SERVER['SERVER_NAME'] . '/echurch/church-admin/' . $path . $dbName;
        $targetPathDB = $path . $dbName;
        /*var_dump($targetPath);
        die(var_dump($sourcePath));*/
        if(move_uploaded_file($sourcePath,$targetPath)) {
            $targetPath = substr($targetPath, 3);
            $p = new Upload($file_title, $file_description, $targetPathDB, Media::table_name);
            $p->setFileType($file_type);
            $result = $p->sendToDbMedia();
            if($result){
                echo '<div class="output">';
                echo "<p>The file was uploaded successfully</p>";
                echo "<p>Your Video or Audio upload is currently being processed and will be available shortly</p>";
                echo " <button type='button' class='btn btn-primary' data-dismiss='modal' onclick='reloadPage();'>Close</button>";
                echo '</div>';
            }
            else{
                echo '<div class="output">';
                echo "<p>Unfortunately an unexpected error ocuur</p>";
                echo " <button type='button' class='btn btn-primary' data-dismiss='modal' onclick='reloadPage();'>Close</button>";
                echo '</div>';
            }
        }else{
            die("An unexpected error occur");
        }
    }
}

