<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 8/7/14
 * Time: 7:08 PM
 * To change this template use File | Settings | File Templates.
 */
require_once("../core/Upload.php");
require_once("../core/check_status.php");

if(isset($_POST['m_title']) && isset($_POST['m_desc'])){
    $file_title = $_POST['m_title'];
    $file_description = $_POST['m_desc'];
}
else{
    echo "Incomplete fields";
    die();
}

if(is_array($_FILES)) {
    if(is_uploaded_file($_FILES['f_name']['tmp_name'])) {
        $sourcePath = $_FILES['f_name']['tmp_name'];
        $targetPath = "../uploads/".$_FILES['f_name']['name'];
        if(move_uploaded_file($sourcePath,$targetPath)) {

        }
    }
}

$p = new Upload($file_title, $file_description, $targetPath, 'newsletters');
$result = $p->sendToDb();
if($result){
    echo "<h2>Successfull</h2>
                <p>Upload was successful</p>";
    echo " <button type='button' class='btn btn-primary' data-dismiss='modal' onclick='reloadPage();'>Close</button>";
}
else{
    echo "<h2>Unfortunately</h2>
                <pAn unexpected error ocuur</p>";
    echo " <button type='button' class='btn btn-primary' data-dismiss='modal' onclick='reloadPage();'>Close</button>";
}
?>