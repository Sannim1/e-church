<?php
/**
 * Created by PhpStorm.
 * User: caleb
 * Date: 8/9/14
 * Time: 1:25 PM
 */

//require_once("../core/check_status.php");
require("../core/query.php");
require("../data_access/DataTable.php");
require("../data_access/db_connection.php");
session_start();

$title = $_REQUEST['title'];
$message = $_REQUEST['message'];
$time = time();

$table = Notifications::table_name;
$field_message = Notifications::message;
$field_title = Notifications::title;

$field_time = Notifications::time;

$sql = "INSERT INTO $table ($field_title, $field_message, $field_time, created_date, modified_date) VALUES ('$title', '$message', $time, NOW(), NOW())";

$query = new Query($sql);

if (!$query->status()){
    $result = "shing";
}
else{
    $result = "success";
}

//echo $result;

//$table = GCM_IDS::table_name;
//$field_clientId = GCM_IDS::clientId;
$table = "app_user";
$field_clientId = "gcm_id";

$sql = "SELECT DISTINCT($field_clientId) FROM $table;";

$query = new Query($sql);
if($query->status()){
    $data = $query->getAll();
//var_dump($data);

    $result = array();

    for($i = 0; $i < sizeof($data); $i++){
        $result[$i] = $data[$i]['gcm_id'];
    }

//------------------------------
// Payload data you want to send to Android device
//    (will be accessible via intent extras)
//------------------------------

    $data = array();
    $data['notification_type'] = MESSAGE;
    $data['subject'] = $title;
    $data['message'] = $message;

//------------------------------
// The recipient registration IDs that will receive the push
//    (Should be stored in your DB)
// Read about it here:// http://developer.android.com/google/gcm/
//------------------------------

    $ids = $result;
//------------------------------
// Call our custom GCM function
//------------------------------
    sendGoogleCloudMessage(  $data, $ids );

}
else{
    echo "shing";
}



//------------------------------
// Define custom GCM function
//------------------------------
function sendGoogleCloudMessage( $data, $ids )
{
    //------------------------------
    // Replace with real GCM API
    // key from Google APIs Console
    //
    // https://code.google.com/apis/console/
    //------------------------------

    $apiKey = 'AIzaSyA5LoIrhz-ViAfP0IOQ51pA7cquyCaT06A';

    //------------------------------
    // Define URL to GCM endpoint
    //------------------------------

    $url = 'https://android.googleapis.com/gcm/send';

    //------------------------------
    // Set GCM post variables
    // (Device IDs and push payload)
    //------------------------------

    $post = array(
        'registration_ids'  => $ids,
        'data'              => $data
    );

    //------------------------------
    // Set CURL request headers
    // (Authentication and type)
    //------------------------------

    $headers = array(
        'Authorization: key=' . $apiKey,
        'Content-Type: application/json'
    );

    //------------------------------
    // Initialize curl handle
    //------------------------------

    $ch = curl_init();

    //------------------------------
    // Set URL to GCM endpoint
    //------------------------------

    curl_setopt( $ch, CURLOPT_URL, $url );

    //------------------------------
    // Set request method to POST
    //------------------------------

    curl_setopt( $ch, CURLOPT_POST, true );

    //------------------------------
    // Set our custom headers
    //------------------------------

    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

    //------------------------------
    // Get the response back as
    // string instead of printing it
    //------------------------------

    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

    //------------------------------
    // Set post data as JSON
    //------------------------------

    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) );

    //------------------------------
    // Actually send the push!
    //------------------------------

    $result = curl_exec( $ch );

    //------------------------------
    // Error? Display it!
    //------------------------------

    if ( curl_errno( $ch ) )
    {
        echo 'GCM error: ' . curl_error( $ch );
    }

    //------------------------------
    // Close curl handle
    //------------------------------

    curl_close( $ch );

    //------------------------------
    // Debug GCM response
    //------------------------------

    echo "success";
}