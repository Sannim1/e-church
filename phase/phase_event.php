<?php
/**
 * Created by PhpStorm.
 * User: caleb
 * Date: 8/22/14
 * Time: 2:08 PM
 */

require_once '../core/query.php';
require("../data_access/DataTable.php");
require_once '../core/check_status.php';
require("../data_access/db_connection.php");
require_once '../core/src/Google_Client.php';
require_once '../core/src/contrib/Google_CalendarService.php';

require_once '../data_access/DataTable.php';
require_once '../data_access/Constants.php';
require_once '../data_access/SqlClient.php';
require_once '../data_access/SqlStatement.php';

require_once '../core/base.php';
require_once '../core/Gcm.php';

if(isset($_REQUEST['name'])){
    $_SESSION['name'] = $_REQUEST['name'];
    $_SESSION['desc'] = $_REQUEST['desc'];
    $_SESSION['location'] = $_REQUEST['location'];

//    $startDate = new DateTime;
    $dummy = getdate(strtotime($_REQUEST['date']));
    $time = explode(':', $_REQUEST['time'], 2);
    $dummy['hours'] = $time[0] - 1;
    $dummy['minutes'] = $time[1];
    $test = mktime($dummy['hours'], $dummy['minutes'], $dummy['seconds'], $dummy['mon'], $dummy['mday'], $dummy['year']);
//    $dummy = $startDate->setTimestamp($test);
    $dummy = date(DATE_ATOM, $test);
    $_SESSION['time'] = $test;
    $_SESSION['start'] = $dummy;

//    $endDate = new DateTime;
    $dummy = getdate($test);
    $dummy['hours']++;
    $test = mktime($dummy['hours'], $dummy['minutes'], $dummy['seconds'], $dummy['mon'], $dummy['mday'], $dummy['year']);
    $dummy = date(DATE_ATOM, $test);
    $_SESSION['end'] = $dummy;

    header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
}

$client = new Google_Client();
$client->setApplicationName("E-Church");

// Visit https://code.google.com/apis/console?api=calendar to generate your
// client id, client secret, and to register your redirect uri.
$client->setClientId('252426667749-tlh7nl4rbikvpg63bmlsi6c77u003fm9.apps.googleusercontent.com');
$client->setClientSecret('Hx3GNw7CDF2D1uk1Z3t0gg3h');
$client->setRedirectUri('http://localhost/echurch/phase/phase_calendar.php');
$client->setDeveloperKey('AIzaSyDnW8q6lXU5dHDO2GRnj2nDs96T56x4Fj8');
$cal = new Google_CalendarService($client);

//if (isset($_GET['logout'])) {
//    unset($_SESSION['token']);
//}

if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $_SESSION['token'] = $client->getAccessToken();
    header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
}

if (isset($_SESSION['token'])) {
    $client->setAccessToken($_SESSION['token']);
}

if ($client->getAccessToken()) {
//    echo json_encode($_SESSION);
    addToDB();
    addToCalendar($client, $cal, getPeople());
} else {
    $authUrl = $client->createAuthUrl();
    header('Location: ' . $authUrl);
}

function addToCalendar($client, $cal, $people){
    //    $calList = $cal->calendarList->listCalendarList();
//    print "<h1>Calendar List</h1><pre>" . print_r($calList, true) . "</pre>";

//    echo substr($_SESSION['end']->format('Y-m-d\TH:i:s.u'), 0, -3);
//    die();

    $event = new Google_Event();
    $event->setSummary($_SESSION['name']);
    $event->setDescription($_SESSION['desc']);
    $event->setLocation($_SESSION['location']);

    $start = new Google_EventDateTime();
    $start->setDateTime($_SESSION['start']);
    $event->setStart($start);

    $end = new Google_EventDateTime();
    $end->setDateTime($_SESSION['end']);
    $event->setEnd($end);

    $attendee1 = new Google_EventAttendee();
    $attendee1->setEmail('fdamilola@gmail.com');
    $attendees = array($attendee1);
    $event->attendees = $attendees;
    $createdEvent = $cal->events->insert('flihkqggv0qjubktdolg497hu0@group.calendar.google.com', $event);

    $newEvent = new Google_Event($createdEvent);
    echo json_encode($newEvent);
//    echo $newEvent->getDescription();

    $_SESSION['token'] = $client->getAccessToken();
}

function addToDB(){
    $name = $_SESSION['name'];
    $desc = $_SESSION['desc'];
    $location = $_SESSION['location'];
    $date = $_SESSION['start'];
    $time = $_SESSION['time'];

    $table = Events::table_name;
    $field_name = Events::name;
    $field_desc = Events::description;
    $field_location = Events::location;
    $field_date = Events::date;
    $field_time = Events::time;

    $sql = "INSERT INTO $table ($field_name, $field_desc, $field_location, $field_date, $field_time, created_date, modified_date)
    VALUES ('$name', '$desc', '$location', '$date', $time, NOW(), NOW())";

    $query = new Query($sql);

    if (!$query->status()){
        $result = "shing";
    }
    else{
        /* Construct GCM parameters */

        //GCM IDs
        $gcm_ids = GCM::getGCM_IDS();

        //GCM payload
        $gcm_payload = array();
        $gcm_payload['notification_type'] = EVENT;
        $gcm_payload[Events::name] = $name;
        $gcm_payload[Events::description] = $desc;
        $gcm_payload[Events::location] = $location;
        $gcm_payload[Events::date] = $date;
        $gcm_payload[Events::time] = $time;

        //Send GCM
        GCM::sendGoogleCloudMessage($gcm_payload, $gcm_ids);

        $result = "success";
    }
}

function getPeople(){
    return null;
}