<?php
error_reporting(0);
  require_once '../data_access/DataTable.php';
  require_once '../data_access/Constants.php';
  require_once '../data_access/SqlStatement.php';
  require_once '../data_access/SqlClient.php';

  require_once '../core/base.php';
  require_once '../core/notification.php';

  require_once '../core/Gcm.php';
  require_once '../utils/JsonResponse.php';

  if (isset($_REQUEST['title'], $_REQUEST['message']) && $_REQUEST['title'] !== '' && $_REQUEST['message'] !== '') {

    $notification_data = array();
    $notification_data[NotificationsTable::title] = $_REQUEST['title'];
    $notification_data[NotificationsTable::message] = $_REQUEST['message'];

    $notification = new Notification($notification_data);
    if ($notification->add()) {
      $notification_data['notification_type'] = MESSAGE;
      if (GCM::sendGoogleCloudMessage($notification_data, GCM::getGCM_IDS())) {
        echo JsonResponse::success('Message posting successful');
        exit();
      } else {
        echo JsonResponse::error('Unable to send GCM!');
        exit();
      }
    }
  } else {
    echo JsonResponse::error('Invalid request parameters!');
    exit();
  }

?>