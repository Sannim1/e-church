<?php
/**
 * Created by PhpStorm.
 * User: caleb
 * Date: 8/22/14
 * Time: 2:08 PM
 */

require_once '../core/query.php';
require("../data_access/DataTable.php");
require_once '../core/check_status.php';
require("../data_access/db_connection.php");
require_once '../core/src/Google_Client.php';
require_once '../core/src/contrib/Google_CalendarService.php';

require_once '../data_access/DataTable.php';
require_once '../data_access/Constants.php';
require_once '../data_access/SqlClient.php';
require_once '../data_access/SqlStatement.php';

require_once '../core/base.php';
require_once '../core/Event.php';
require_once '../core/User.php';
require_once '../core/Gcm.php';

if(isset($_REQUEST['name'])){
    $_SESSION['name'] = $_REQUEST['name'];
    $_SESSION['desc'] = $_REQUEST['desc'];
    $_SESSION['location'] = $_REQUEST['location'];

//    $startDate = new DateTime;
    $dummy = getdate(strtotime($_REQUEST['date']));
    $time = explode(':', $_REQUEST['time'], 2);
    $dummy['hours'] = $time[0] - 1;
    $dummy['minutes'] = $time[1];
    $test = mktime($dummy['hours'], $dummy['minutes'], $dummy['seconds'], $dummy['mon'], $dummy['mday'], $dummy['year']);
//    $dummy = $startDate->setTimestamp($test);
    $dummy = date(DATE_ATOM, $test);
    $_SESSION['time'] = $test;
    $_SESSION['start'] = $dummy;

//    $endDate = new DateTime;
    $dummy = getdate($test);
    $dummy['hours']++;
    $test = mktime($dummy['hours'], $dummy['minutes'], $dummy['seconds'], $dummy['mon'], $dummy['mday'], $dummy['year']);
    $dummy = date(DATE_ATOM, $test);
    $_SESSION['end'] = $dummy;

    header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
}

$client = new Google_Client();
$client->setApplicationName("E-Church");

// Visit https://code.google.com/apis/console?api=calendar to generate your
// client id, client secret, and to register your redirect uri.
$client->setClientId('252426667749-tlh7nl4rbikvpg63bmlsi6c77u003fm9.apps.googleusercontent.com');
$client->setClientSecret('Hx3GNw7CDF2D1uk1Z3t0gg3h');
$client->setRedirectUri('http://localhost/echurch/phase/phase_calendar.php');
$client->setDeveloperKey('AIzaSyDnW8q6lXU5dHDO2GRnj2nDs96T56x4Fj8');
$cal = new Google_CalendarService($client);

//if (isset($_GET['logout'])) {
//    unset($_SESSION['token']);
//}

if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $_SESSION['token'] = $client->getAccessToken();
    header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
}

if (isset($_SESSION['token'])) {
    $client->setAccessToken($_SESSION['token']);
}

if ($client->getAccessToken()) {
//    echo json_encode($_SESSION);
    if (addToDB()) {
        addToCalendar($client, $cal, getPeople());
        header('location: ../event_scheduling.php');
    }
    //addToDB();
    //addToCalendar($client, $cal, getPeople());
} else {
    $authUrl = $client->createAuthUrl();
    header('Location: ' . $authUrl);
}

function addToDB(){
    $data = array();

    $data[EventsTable::name] = $_SESSION['name'];
    $data[EventsTable::description] = $_SESSION['desc'];
    $data[EventsTable::location] = $_SESSION['location'];
    $data[EventsTable::date] = $_SESSION['start'];
    $data[EventsTable::time] = $_SESSION['time'];

    $event = new Event($data);

    if ($event->add()) {
        /* Construct GCM parameters */

        //GCM IDs
        $gcm_ids = GCM::getGCM_IDS();

        //GCM payload
        $data['notification_type'] = EVENT;

        //Send GCM
        if (GCM::sendGoogleCloudMessage($data, $gcm_ids)) {
            return true;
        }
    } else {
        return false;
    }
}

function addToCalendar($client, $cal, $people){

    $event = new Google_Event();
    $event->setSummary($_SESSION['name']);
    $event->setDescription($_SESSION['desc']);
    $event->setLocation($_SESSION['location']);

    $start = new Google_EventDateTime();
    $start->setDateTime($_SESSION['start']);
    $event->setStart($start);

    $end = new Google_EventDateTime();
    $end->setDateTime($_SESSION['end']);
    $event->setEnd($end);

    $attendees = array();

    foreach ($people as $row) {
        $attendee = new Google_EventAttendee();
        $attendee->setEmail($row['email']);
        array_push($attendees, $attendee);
    }

    $event->attendees = $attendees;
    $createdEvent = $cal->events->insert('flihkqggv0qjubktdolg497hu0@group.calendar.google.com', $event);

    $newEvent = new Google_Event($createdEvent);
    echo json_encode($newEvent);
//    echo $newEvent->getDescription();

    $_SESSION['token'] = $client->getAccessToken();
}

function getPeople(){
    return User::getEmails();
}