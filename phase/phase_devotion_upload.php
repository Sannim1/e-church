<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 8/7/14
 * Time: 7:08 PM
 * To change this template use File | Settings | File Templates.
 */
require_once("../core/Upload.php");
require_once("../core/check_status.php");

if(isset($_POST['m_title']) && isset($_POST['m_desc'])){
    $file_title = $_POST['m_title'];
    $file_description = $_POST['m_desc'];
}
else{
    die("Incomplete fields");

}

if(is_array($_FILES)) {
    $file_size = $_FILES['f_name']['size'];
    if($file_size > 2097152){
        echo '<div class="output">';
        echo "<p>The file size is to large</p>";
        echo " <button type='button' class='btn btn-primary' data-dismiss='modal' onclick='reloadPage();'>Close</button>";
        echo '</div>';
    }
    else{
        $sourcePath = trim($_FILES['f_name']['tmp_name']);

        $filename = $_FILES['f_name']['name'];
        $targetPath = "../devotions/".$_FILES['f_name']['name'];
        if(move_uploaded_file($sourcePath,$targetPath)) {
            $p = new Upload($file_title, $file_description, $filename, 'devotions');
            $result = $p->sendToDb();
            if($result){
                echo '<div class="output">';
                echo "<p>The file was uploaded successfully</p>";
                echo " <button type='button' class='btn btn-primary' data-dismiss='modal' onclick='reloadPage();'>Close</button>";
                echo '</div>';
            }
            else{
                echo '<div class="output">';
                echo "<p>Unfortulately an error occur</p>";
                echo " <button type='button' class='btn btn-primary' data-dismiss='modal' onclick='reloadPage();'>Close</button>";
                echo '</div>';
            }
        }else{
            die("An unexpected error occur");
        }
    }
}


?>