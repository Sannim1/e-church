<?php
//require_once("data_access/DataTable.php");
require_once("data_access/db_connection.php");
require_once("core/check_status.php");

require_once("core/Media.php");
require_once("core/query.php");
require_once("data_access/Constants.php");

require("phase/info.php");
?>

<!DOCTYPE HTML>

<html>
<head>
    <title><?php echo $data['data']['church_name']; ?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" href="css/mystyle.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.dropotron.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/skel-layers.min.js"></script>
    <script src="js/init.js"></script>
    <script src="js/myJs.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/media.js"></script>

    <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
    </noscript>
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->

</head>
<body>

<!-- Header -->
<div id="header">

    <!-- Logo -->
    <h1><a href="profile.php" id="logo"><?php echo $data['data']['church_name']?> E-CHURCH <em>App</em></a></h1>

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="profile.php">Profile</a></li>
            <li><a href="notifications.php">Notifications</a></li>
            <li class="current"><a href="media.php">Media</a></li>
            <li><a href="event_scheduling.php">Events</a></li>
            <li>
                <a href="">Inspirational</a>
                <ul>
                    <li><a href="newsletter.php">NewsLetter</a></li>
                    <li><a href="devotions.php">Devotions</a></li>
                </ul>
            </li>
            <li>
                <a class="signout" href="signout.php">Sign out</a>
            </li>
        </ul>
    </nav>

</div>

<section class="wrapper2 style1">
    <div class="upload_button">
        <input type="button" class="button alt" value="Upload Media" data-toggle="modal" data-target="#uploadModal" />
    </div>
</section>

<!-- Main -->
<section class="wrapper style1">
    <div class="container">
        <div class="row double">
            <div class="3u">
                <div id="sidebar1">

                    <!-- Sidebar 1 -->

                    <section>
                        <h2>Audio</h2>
                    </section>
                    <hr>
                    <section>
                        <?php
                        $upload_audio = new Media(1);
                        $records = $upload_audio->getRecords();
                        //var_dump($records);
                        if(sizeof($records) == 0){
                            echo "<div class='emptyRecord'><p>Empty record</p></div>";
                        }
                        else{
                            foreach($records as $record){
                                ?>
                                <div class="files">
                                    <div class="delete" id="<?php echo $record['id']; ?>" title="<?php echo trim($record['file_name']); ?>">&times</div>
                                    <button onclick="showAudio(this)" data-url="<?php echo BASEURL . trim($record['file_name']) ?>"class="btn btn-default btn-sm">View</button>
                                    <h3><?php echo $record['file_title']; ?></h3>
                                    <p>
                                        <?php  echo $record['description'] ?>
                                    </p>
                                </div>
                            <?php
                            }
                        }
                        ?>
                    </section>

                </div>
            </div>
            <div class="6u skel-cell-important">
                <div id="content">

                    <!-- Content -->

                    <article class="videos">
                        <header>
                            <h2>Videos</h2>
                        </header>
                        <?php
                        $media = new Media(2);
                        $records = $media->getRecords();
                        //var_dump($records);
                        if(sizeof($records) == 0){
                            echo "<div class='emptyRecord'><p>Empty record</p></div>";
                        }
                        else{
                            foreach($records as $record){
                                //                           var_dump($record);
                                ?>
                                <div class="files">
                                    <div class="delete" id="<?php echo $record['id']; ?>" title="<?php echo trim($record['file_name']); ?>">&times</div>
                                    <button onclick="showVideo(this)" data-url="<?php echo BASEURL . trim($record['file_name']) ?>"class="btn btn-default btn-sm">View</button>
                                    <h3><?php echo $record['file_title']; ?></h3>
                                    <p>
                                        <?php  echo $record['description'] ?>
                                    </p>
                                </div>
                            <?php
                            }
                        }
                        ?>

                    </article>

                </div>
            </div>
            <div class="3u">
                <div id="sidebar2">

                    <!-- Sidebar 2 -->

                    <section>
                        <h2>Pictures</h2>
                        <?php
                        $upload_pictures = new Media(0);
                        $records = $upload_pictures->getRecords();
                        //var_dump($records);
                        if(sizeof($records) == 0){
                            echo "<div class='emptyRecord'><p>Empty record</p></div>";
                        }
                        else{
                            foreach($records as $record){
                                //                           var_dump($record);
                                ?>
                                <div class="files">
                                    <div class="delete" id="<?php echo $record['id']; ?>" title="<?php echo trim($record['file_name']); ?>">&times</div>
                                    <button onclick="showPicture(this)" data-url="<?php echo BASEURL . trim($record['file_name']) ?>"class="btn btn-default btn-sm">View</button>
                                    <h3><?php echo $record['file_title']; ?></h3>
                                    <p><?php  echo $record['description'] ?></p>
                                </div>
                            <?php
                            }
                        }
                        ?>

                    </section>



                </div>
            </div>
        </div>
    </div>
</section>

<!-- Upload  modal-->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">

        <form  name="uploadMedia" class="form-group" id="mForm" action="phase/media.php" method="post" onsubmit="return false">
            <div class="modal-content">
                <div id="response">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="signUpModalLabel">Upload</h4>
                    </div>
                    <div class="modal-body">

                        <div class="errorMsg" id="uploadError"></div>
                        <input class="form-control ejo" placeholder="Media Title" name="m_title">
                        <select class="form-control" name="m_type" id="m_type">
                            <option value="0">Picture</option>
                            <option value="1">Audio</option>
                            <option value="2">Video</option>
                        </select>
                        <textarea class="form-control ejo" placeholder="Media Description" name="m_desc"></textarea>

                        <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-primary btn-file">
                                Browse&hellip; <input type="file" multiple name="f_name">
                            </span>
                        </span>
                            <input  id="m_name" type="text" name="m_name" class="form-control" readonly>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" onclick="uploadMediaForm()">Upload</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>

<!-- Video  modal-->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="videoLabel">Playing Video</h4>
            </div>

            <div class="modal-body">
                <div id="html">
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<div id="footer">
    <!-- Icons -->
    <ul class="icons">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa-github"><span class="label">GitHub</span></a></li>
        <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
    </ul>

    <!-- Copyright -->
    <div class="copyright">
        <ul class="menu">
            <li>&copy; eChurch All rights reserved</li>
        </ul>
    </div>

</div>
</body>
</html>