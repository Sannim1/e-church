<?php
require('phase/get_data.php');
//require('phase/get_events.php');
?>

<!DOCTYPE HTML>

<html>
<head>
    <title>Events</title>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/mystyle.css">
    <link rel="stylesheet" href="css/events.css">
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->

    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.dropotron.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/skel-layers.min.js"></script>
    <script src="js/init.js"></script>
    <script src="js/myJs.js"></script>
    <script src="js/events.js"></script>

    <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
    </noscript>

</head>
<body>

<!-- Header -->
<div id="header">


    <!-- Logo -->
    <h1><a href="profile.php" id="logo"><?php echo $result[SignUpTable::church_name]?> CHURCH <em>App</em></a></h1>

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="profile.php">Profile</a></li>
            <li><a href="notifications.php">Notifications</a></li>
            <li><a href="media.php" class="current">Media</a></li>
            <li class="current"><a href="event_scheduling.php">Events</a></li>
            <li>
                <a href="">Inspirational</a>
                <ul>
                    <li><a href="newsletter.php">NewsLetter</a></li>
                    <li><a href="devotions.php">Devotions</a></li>
                </ul>
            </li>
            <li>
                <a class="signout" href="signout.php">Sign out</a>
            </li>
        </ul>
    </nav>

</div>

<!-- Main -->
<section class="wrapper style1">
    <div class="container">
        <!-- Content -->
        <div id="content">
            <article>
                <div class="panel panel-default">
                    <form onsubmit="postEvent(); return false;">
                        <div class="panel-body">
                            <div id="response">
                                <div class="errorMsg" id="msg"></div>
                                <input class="form-control ejo" placeholder="Event Name" id="name">
                                <input class="form-control ejo" placeholder="Event Description" id="desc">
                                <input class="form-control ejo" placeholder="Event Location" id="location">
                                <input class="form-control ejo" type="date" placeholder="Event Date" id="date">
                                <select class="form-control ejo" id="time">
                                    <option value="">Event Time 24 hr format eg 12:00</option>
                                    <?php
                                    for($i = 0; $i < 24; $i++){
                                        $i = strval($i);
                                        if(strlen($i) < 2){
                                            $time = "0" . $i . ":00";
                                        }
                                        else{
                                            $time = $i . ":00";
                                        }

                                        echo "<option value='$time'>$time</option>";
                                    }
                                    ?>
                                </select>

                                <div class="panel-footer">
                                    <button type="submit" class="btn btn-primary" >Post Event</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id="content">
            <!-- Content -->
            <div class="col-xs-12">
                <ul class="event-list">

                </ul>
            </div>
        </div>
        <div class="col-md-12 load_panel">
            <h3 class="text-center"><span class="cursor load" id="load_more">load more</span></h3>
        </div>
    </div>
</section>

<!-- Footer -->
<div id="footer">

    <!-- Icons -->
    <ul class="icons">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa-github"><span class="label">GitHub</span></a></li>
        <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
    </ul>

    <!-- Copyright -->
    <div class="copyright">
        <ul class="menu">
            <li>&copy; eCHURCH. All rights reserved</li>
        </ul>
    </div>
</div>
<script src="js/bootstrap.js"></script>
</body>
</html>