
var bustcachevar=1; //bust potential caching of external pages after initial request? (1=yes, 0=no)
var bustcacheparameter="";
var name;
var email;
var subject;


function createRequestObject(){
    try	{
        xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    }	catch(e)	{
        alert('Sorry, but your browser doesn\'t support XMLHttpRequest.');
    };
    return xmlhttp;
};

function ajaxpage(url, containerid){
    var page_request = createRequestObject();

    if (bustcachevar) bustcacheparameter=(url.indexOf("?")!=-1)? "&"+new Date().getTime() : "?"+new Date().getTime()
    page_request.open('GET', url+bustcacheparameter, true)
    page_request.send(null)

    page_request.onreadystatechange=function(){
        loadpage(page_request, containerid)
    }

}
function loadpage(page_request, containerid){
    if (page_request.readyState == 4 && (page_request.status==200 || window.location.href.indexOf("http")==-1)) {
        document.getElementById(containerid).innerHTML=page_request.responseText;
    };
}

//styling the input type file
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);

});
$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }

    });
});



function submitLoginForm(){

    var email = $('#ch_email').val();
    var pass = $('#password').val();

    console.log(email);
    console.log(pass);

    if (email !== '' && pass  !== '') {

        $.post("phase/signin.php",
            {
                email: email,
                pass: pass
            },
            function(result){
                console.log(result);
                if(result == -1){
                    $('#sign-error').removeClass('hidden')
                    $('#sign-error').html('Invalid Credentials');
                }else{
                    console.log(result);
                    window.location.assign('profile.php');
                }

            }
        );
    } else {
        document.getElementById('sign-error').innerHTML = 'Fill in all fields';
    }
    return false;
}




//submit form asynchronously

function submitRegForm(){

    if (document.signUp.c_name.value !== '' && document.signUp.c_email.value !== '' && document.signUp.password.value !== '' & document.signUp.p_name.value !== ''  && document.signUp.p_number.value !== '' && document.signUp.p_email.value !== '') {
        if(validateEmail(document.signUp.c_email.value)){
            if(validateEmail(document.signUp.p_email.value)){
                ajaxpage("phase/registration.php?do=signUp&ch_name=" + encodeURI(document.signUp.c_name.value) +
                    "&ch_email=" + encodeURI(document.signUp.c_email.value) +
                    "&pswrd=" + encodeURI(document.signUp.password.value) +
                    "&pst_name=" + encodeURI(document.signUp.p_name.value) +
                    "&pst_number=" + encodeURI(document.signUp.p_number.value) +
                    "&pst_email=" + encodeURI(document.signUp.p_email.value),  "response");
            }
            else if(!validateEmail(document.signUp.p_email.value)){
                document.getElementById('regError').innerHTML = 'Invalid pastor  email';
            }

        }
        else if(!validateEmail(document.signUp.c_email.value)){
            document.getElementById('regError').innerHTML = 'Invalid church email';
        }
    }

    else {
        document.getElementById('regError').innerHTML = 'Incomplete fields';
    }

}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}








function reloadPage(){
    window.location.reload();
}
//signin

$(function() {
    $('#create').poptrox({
        usePopupCaption: false,
        usePopupCloser: false,
        usePopupNav:false,
        popupPadding: 5,
        useBodyOverflow: true
    });

//    PopOver
    $("[data-toggle='popover']").each(function(index, element) {
        var contentElementId = $(element).data().target;
        var contentHtml = $(contentElementId).html();
        $(element).popover({
            content: contentHtml,
            html: true,
            animation: true,
            placement: 'bottom',
            container: 'body'

        });
    });

//    ToolTip
    $("[data-toggle='tooltip']").tooltip();
});

function acceptTerms(e){
    $(e).addClass('disabled');
    $(e).attr('disabled', 'disabled');
    $('#proceed').removeClass('disabled');
    $('#proceed').removeAttr('disabled');
}

function proceed(){
    $('#terms').hide('slow');
    $('#signUpDiv').removeClass('hidden');
}

