<?php
error_reporting(E_ERROR);

require('phase/get_data.php');


?>
<!DOCTYPE HTML>

<html>
<head>
    <title><?php echo $result[SignUpTable::church_name]?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="E-Church Portal for <?php echo $result[SignUpTable::church_name] ?>" />
    <meta name="keywords" content="E-Church" />
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/mystyle.css">
    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.dropotron.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/skel-layers.min.js"></script>
    <script src="js/init.js"></script>
    <script src="js/myJs.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnW8q6lXU5dHDO2GRnj2nDs96T56x4Fj8">
    </script>
    <script src="js/map.js"></script>
    <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
    </noscript>
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
</head>
<body>
<!-- Header -->
<div id="header">
    <!-- Logo -->
    <h1><a href="profile.php" id="logo"><?php echo $result[SignUpTable::church_name]?> E-CHURCH <em>App</em></a></h1>

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li class="current"><a href="profile.php">Profile</a></li>
            <li><a href="notifications.php">Notifications</a></li>
            <li><a href="media.php" class="current">Media</a></li>
            <li><a href="event_scheduling.php">Events</a></li>
            <li>
                <a href="">Inspirational</a>
                <ul>
                    <li><a href="newsletter.php">NewsLetter</a></li>
                    <li><a href="devotions.php">Devotions</a></li>
                </ul>
            </li>
            <li>
                <a class="signout" href="signout.php">Sign out</a>

            </li>

        </ul>

    </nav>

</div>

<!-- Main -->
<section class="wrapper style1">
    <div class="container">
        <div class="row double">
            <div class="4u">
                <div id="content">
                    <section>
                        <h3>Edit profile</h3>
                        <ul class="navbar-left">
                            <li><a href="#" class="form-control ejo" data-toggle="modal" data-target="#profileModal">Church Profile</a></li>
                            <li><a href="#" class="form-control ejo" onclick="initialize('<?php echo $result[SignUpTable::church_location]; ?>')">Church Location</a></li>
                            <li><a href="#" class="form-control ejo"data-toggle="modal" data-target="#pastorModal">Pastor's Contact Details</a></li>
                            <li><a href="#" class="form-control ejo" data-toggle="modal" data-target="#passwordModal">Change Admin Password</a></li>
                        </ul>
                    </section>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="8u">
                <div id="sidebar">
                    <!-- Content -->
                    <section>
                        <h2>Church Profile</h2>
                        <p><?php echo $result[SignUpTable::church_name] ?></p>
                        <p id="location"></p>
                        <p><?php echo $result[SignUpTable::description] ?></p>
                        <p><?php echo $result[SignUpTable::pastor_name] ?></p>
                        <p><?php echo $result[SignUpTable::pastor_email] ?></p>
                    </section>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Reverse Geocoding script -->
<script>
    $(document).ready = codeLatLng('<?php echo $result[SignUpTable::church_location] ?>');
</script>

<!-- Entire Profile Modal -->
<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form name="profile" class="form-group">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="contactModalLabel">Edit the Church Profile</h4>
                </div>
                <div class="modal-body">
                    <div class="text-danger text-center" id="profile-error"></div>
                    <div class="text-success text-center" id="profile-success"></div>
                    <div class="is-loading hidden">
                        <img src=" images/loading.gif">
                    </div>
                    <div class="form-group">
                        <label>Church Name</label>
                        <input class="form-control" name="name"  value="<?php echo $result[SignUpTable::church_name] ?>">
                    </div>
                    <div class="form-group">
                        <label>Church Vision</label>
                        <input class="form-control" name="vision" value="<?php echo $result[SignUpTable::vision] ?>">
                    </div>
                    <div class="form-group">
                        <label>Church Mission</label>
                        <input class="form-control" name="mission" value="<?php echo $result[SignUpTable::mission] ?>">
                    </div>
                    <div class="form-group">
                        <label>Church description</label>
                        <textarea class="form-control" name="description"><?php echo $result[SignUpTable::description] ?></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="updateChurchProfile()">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Location Modal -->
<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form name="location" class="form-group" onsubmit="codeAddress(); return false;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="contactModalLabel">Specify the Church Location</h4>
                </div>
                <div class="modal-body">
                    <div class="errorMsg" id="msg"></div>
                    <input class="form-control ejo" name="address" placeholder="Enter new church address or search for a location on the map">
                    <div id="map-canvas" class="form-control ejo" style="height: 500px;"></div>
                </div>
                <div class="modal-footer">
                    <button id="search" class="btn btn-info" onclick="codeAddress()">Search</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" onclick="submitLocation()">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Pastor's Contact Modal -->
<div class="modal fade" id="pastorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form name="pastor" class="form-group">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="pastorModalLabel">Edit the Pastor's Profile</h4>
                </div>
                <div class="modal-body">
                    <div class="is-loading hidden">
                        <img src=" images/loading.gif">
                    </div>
                    <div class="text-danger text-center" id="pst_error"></div>
                    <div class="text-success text-center" id="pst_success"></div>
                    <div class="form-group">
                        <label>Pastor's Name</label>
                        <input class="form-control" name="name" value="<?php echo $result[SignUpTable::pastor_name] ?>">
                    </div>
                    <div class="form-group">
                        <label>Pastor's Number</label>
                        <input class="form-control" name="number" value="<?php echo $result[SignUpTable::pastor_number] ?>">
                    </div>
                    <div class="form-group">
                        <label>Pastor's Email</label>
                        <input class="form-control" name="email" value="<?php echo $result[SignUpTable::pastor_email] ?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="updatePastorProfile()">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Change Password Modal -->
<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form name="admin" class="form-group">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="passwordModalLabel">Change Admin Password</h4>
                </div>
                <div class="modal-body">
                    <div class="is-loading hidden">
                        <img src="images/loading.gif">
                    </div>
                    <div class="text-danger text-center" id="chg_psw_error"></div>
                    <div class="text-success text-center" id="chg_psw_success"></div>
                    <div class="form-group">
                        <label>Username</label>
                        <input class="form-control" name="username" value="<?php echo $result[SignUpTable::church_email]; ?>">
                    </div>
                    <div class="form-group">
                       <label>Password</label>
                        <input class="form-control" name="password" value="<?php echo $result[SignUpTable::password]; ?>" type="password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="changePassword()">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Footer -->
<div id="footer">
    <!-- Icons -->
    <ul class="icons">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa-github"><span class="label">GitHub</span></a></li>
        <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
    </ul>

    <!-- Copyright -->
    <div class="copyright">
        <ul class="menu">
            <li>&copy; eChurch. All rights reserved</li>
        </ul>
    </div>

</div>
<script src="js/bootstrap.js"></script>
</body>
</html>