<!DOCTYPE HTML>
<html>
<head>
    <title>eChurch</title>
    <meta name="google-site-verification" content="F8_pgTKzDD4KUBEe9rzuCNuaX1hW-PTD-QWKyWkP0yg" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" href="home_resources/custom-bootstrap/css/bootstrap.min.css">

    <!--[if lte IE 8]><script src="home_resources/css/ie/html5shiv.js"></script><![endif]-->
    <script src="home_resources/js/jquery.min.js"></script>
    <script src="home_resources/js/jquery.poptrox.min.js"></script>
    <script src="home_resources/js/jquery.scrolly.min.js"></script>
    <script src="home_resources/js/jquery.scrollgress.min.js"></script>
    <script src="home_resources/js/skel.min.js"></script>
    <script src="home_resources/js/init.js"></script>
    <script src="home_resources/js/homepage.js"></script>
    <script src="home_resources/custom-bootstrap/js/bootstrap.min.js"></script>
    <noscript>
        <link rel="stylesheet" href="home_resources/css/skel.css" />
        <link rel="stylesheet" href="home_resources/css/style.css" />
        <link rel="stylesheet" href="home_resources/css/style-wide.css" />
        <link rel="stylesheet" href="home_resources/css/style-normal.css" />
    </noscript>
    <!--[if lte IE 8]><link rel="stylesheet" href="home_resources/css/ie/v8.css" /><![endif]-->
</head>
<body>

<!-- Header -->
<header id="header">

    <!-- Logo -->
    <a href="#home">
        <span id="logo"><span class="fa fa-home danger_text">&nbsp;</span> eChurch</span>
    </a>
    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="#about">About</a></li>
            <li><a href="#why">Why eChurch?</a></li>
            <li><a href="#pricing">Pricing</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="create mini" class="mini"><a  style="cursor: pointer" data-target="#login-content" data-toggle="popover">Sign in</a></li>
            <li id="create" class="create mini"><a href="signup.php" data-poptrox="ajax,950x700">Get Started</a></li>
        </ul>
    </nav>

</header>

<!-- Intro -->
<section id="home" class="main style1 dark fullscreen">
    <div class="content container small">
        <div class="header-content">
            <header>
                <h2>eChurch</h2>
            </header>
            <p>
                A Cross-Platform Mobile Application Management System for Churches
            </p>
            <p>
                ...the church just got mobile
            </p>
        </div>


        <footer>
            <a href="#about" class="button style2 down">More</a>
        </footer>
    </div>
</section>

<section id="about" class="main style2 right dark fullscreen">
    <div class="content box style2">
        <header>
            <h2>About eCHURCH?</h2>
        </header>
        <p>
            Provision of a single point for mobile application management through a user friendly control panel to provide mobile users with a unique experience.
        </p>
    </div>
    <a href="#why" class="button style2 down anchored">Next</a>
</section>

<!-- Two -->
<section id="why" class="main style2 left dark fullscreen">
    <div class="content box style2">
        <header>
            <h2>Why eCHURCH?</h2>
        </header>
        <ul>
            <li><span class="fa fa-check">&nbsp;</span>The church can be more than the location</li>
            <li><span class="fa fa-check">&nbsp;</span> Expansion of the gospel </li>
        </ul>
    </div>
    <a href="#work" class="button style2 down anchored">Next</a>
</section>

<!-- Work -->
<section id="work" class="main style3 primary">

    <div class="content container">
        <iframe width="850" height="400" src="//www.youtube.com/embed/DdwqrviELeY" frameborder="0" allowfullscreen></iframe>
    </div>
    <a href="#pricing" class="button style2 down anchored">Next</a>

</section>

<!-- Pricing -->
<section id="pricing" class="main style3 secondary">
<div class="content container">
<header>
    <h2>Pricing</h2>
</header>
<div class="box container small pricing">
<!--            Basic Pricing    -->
<div class="price_box basic">
    <div class="title">Basic</div>
    <div class="features">
        <ul>
            <li>
                <a href="#" data-toggle="tooltip" title="Every user will be able to read the church newsletter offline from the application">
                    <span class="fa fa-check">&nbsp;</span>Newsletter</span>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="Scheduling church events and also a source of spreading announcements about such">
                    <span class="fa fa-check">&nbsp;</span>Event Manager</span>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="Another source of announcements or just communicating with the general church populace">
                    <span class="fa fa-check">&nbsp;</span>Post Schedulling</span>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="A location marker with the church’s location">
                    <span class="fa fa-check">&nbsp;</span>Map</span>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="A contact pastor function that allows members send messages to the pastor (or church admin)">
                    <span class="fa fa-check">&nbsp;</span>Reachout</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="price_figure">
        &#x24;20/month  <br/><div class="smallText danger_text">Free for First Year</div>
    </div>
</div>

<!--            Standard Pricing-->
<div class="price_box standard">
    <div class="title">Standard</div>

    <div class="features">
        <ul>
            <li>
                <a href="#" data-toggle="tooltip" title="Every user will be able to read the church newsletter and devotion offline from the application">
                    <span class="fa fa-check">&nbsp;</span>Devotion/Newsletter
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="A connection to the church’s existing blog service">
                    <span class="fa fa-check">&nbsp;</span>Blog Integration
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="For pictures, audio messages and videos.">
                    <span class="fa fa-check">&nbsp;</span>Media Manager
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="Scheduling church events and also a source of spreading announcements about such">
                    <span class="fa fa-check">&nbsp;</span>Event Manager
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="Another source of announcements or just communicating with the general church populace">
                    <span class="fa fa-check">&nbsp;</span>Post Scheduling
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="A location marker with the church’s location">
                    <span class="fa fa-check">&nbsp;</span>Map
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="A contact pastor function that allows members send messages to the pastor (or church admin)">
                    <span class="fa fa-check">&nbsp;</span>Reachout
                </a>
            </li>

        </ul>
        <div class="price_figure">
            &#x24;50 /month
        </div>
    </div>
</div>

<!--            Premium Pricing-->
<div class="price_box premium">
    <div class="title">Premium</div>
    <div class="features">
        <ul>
            <li>
                <a href="#" data-toggle="tooltip" title="Every user will be able to read the church newsletter and devotion offline from the application">
                    <span class="fa fa-check">&nbsp;</span>Devotion/Newsletter
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="A connection to the church’s existing blog service">
                    <span class="fa fa-check">&nbsp;</span>Blog Integration
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="For pictures, audio messages and videos.">
                    <span class="fa fa-check">&nbsp;</span>Media Manager
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="Scheduling church events and also a source of spreading announcements about such">
                    <span class="fa fa-check">&nbsp;</span>Event Manager
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="Another source of announcements or just communicating with the general church populace">
                    <span class="fa fa-check">&nbsp;</span>Post Scheduling
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="View sermons by series and featured topics and share sermons you watch with friends via email too.">
                    <span class="fa fa-check">&nbsp;</span>Sermons
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="e-payment of tithes and offerings">
                    <span class="fa fa-check">&nbsp;</span>Online Giving
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="A navigation from where you are to the church branch.">
                    <span class="fa fa-check">&nbsp;</span>Maps <span class="danger_text"><strong>++</strong></span>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="Prayer requests, testimony, enquiry and general communication amongst all church members.">
                    <span class="fa fa-check">&nbsp;</span>Reachout <span class="danger_text"><strong>++</strong></span>
                </a>
            </li>

        </ul>
        <div class="price_figure">
            &#x24;120 /month
        </div>
    </div>
</div>

<!--            MVP Pricing-->
<div class="price_box mvp">
    <div class="title">MVP</div>
    <div class="features">
        <ul>
            <li>
                <a href="#" data-toggle="tooltip" title="Every user will be able to read the church newsletter and devotion offline from the application">
                    <span class="fa fa-check">&nbsp;</span>Devotion/Newsletter
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="A connection to the church’s existing blog service">
                    <span class="fa fa-check">&nbsp;</span>Blog Integration
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="For pictures, audio messages and videos.">
                    <span class="fa fa-check">&nbsp;</span>Media Manager
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="Scheduling church events and also a source of spreading announcements about such">
                    <span class="fa fa-check">&nbsp;</span>Event Manager
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="Another source of announcements or just communicating with the general church populace">
                    <span class="fa fa-check">&nbsp;</span>Post Scheduling
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="View sermons by series and featured topics and share sermons you watch with friends via email too.">
                    <span class="fa fa-check">&nbsp;</span>Sermons
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="e-payment of tithes and offerings">
                    <span class="fa fa-check">&nbsp;</span>Online Giving
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="A navigation from where you are to the church branch.">
                    <span class="fa fa-check">&nbsp;</span>Maps <span class="danger_text"><strong>++</strong></span>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="Prayer requests, testimony, enquiry and general communication amongst all church members.">
                    <span class="fa fa-check">&nbsp;</span>Reachout <span class="danger_text"><strong>++</strong></span>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="">
                    <span class="fa fa-check">&nbsp;</span>Live Streaming
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="Quizzes, bible stories">
                    <span class="fa fa-check">&nbsp;</span>Children Dept.
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tooltip" title="">
                    <span class="fa fa-check">&nbsp;</span>Notepad
                </a>
            </li>
        </ul>
        <div class="price_figure">
            &#x24;250 /month
        </div>
    </div>
</div>

<div class="clearFix"></div>
</div>
</div>
</section>

<!-- Modal -->
<div class="modal fade" id="signUpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Contact -->
<section id="contact" class="main style3 secondary">
    <div class="content container">
        <header>
            <h2>Need Help? Ask our Team</h2>
            <p>Get surrounded by experts eager to give a hand</p>
        </header>
        <div class="box container small">

            <!-- Contact Form -->
            <form method="post" action="#">
                <div class="row half">
                    <div class="6u"><input type="text" name="name" placeholder="Name" /></div>
                    <div class="6u"><input type="email" name="email" placeholder="Email" /></div>
                </div>
                <div class="row half">
                    <div class="12u"><textarea name="message" placeholder="Message" rows="6"></textarea></div>
                </div>
                <div class="row">
                    <div class="12u">
                        <ul class="actions">
                            <li><input type="submit" value="Send Message" /></li>
                        </ul>
                    </div>
                </div>
            </form>

        </div>
    </div>
</section>

<!-- Footer -->
<footer id="footer">

    <!-- Icons -->
    <ul class="actions">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
    </ul>

    <!-- Menu -->
    <ul class="menu">
        <li>eCHURCH &copy; 2015  All Rights Reserved </li>
    </ul>

</footer>
<script type="text/html" id="login-content">
    <div id="form">
        <form id="login-data" method="post" onsubmit="return false;">
            <div class="hidden" id="sign-error"></div>
            <div class="row half">
                <div class="12u">
                    <input placeholder="Email Address" id="ch_email" type="email" name="ch_email">
                </div>
            </div>
            <div class="row half">
                <div class="12u">
                    <input placeholder="Password" id="password" type="password" name="pswrd">
                </div>
            </div>
            <div class="row half">
                <div class="12u text-center">
                    <input type="submit" value="Sign in" onclick="submitLoginForm()" />
                </div>
            </div>
        </form>
    </div>
</script>
</body>

</html>