<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 9/4/14
 * Time: 1:24 PM
 * To change this template use File | Settings | File Templates.
 */
require_once("data_access/db_connection.php");
require_once("core/check_status.php");

require_once("core/Media.php");

if(isset($_POST['media_id'])){
    $file_id =  $_POST['media_id'];
    $upload_pdf = new Media(1);
    $res = Media::deleteRecord($file_id);

    //deleting the file
    $path = $_POST['media_name'];
    $path = substr($path, -26);
    if(unlink($path)){
        echo "Record deleted successfully";
    }
    else{
        echo "Record not deleted";
    }

}