/**
 * Created by olajuwon on 11/3/14.
 */
var load_offset = 0;
$('document').ready(function(){
    var view_bar = document.getElementsByClassName('view');
    for(i = 0; i <view_bar.length; i++){
        view_bar[i].addEventListener("click",function(){
            load_notification(this);
        }, false)
    }


    $('#load_more').click(function(){
        load_events();
    });

    load_events();
});

function load_events(){
    $.post("phase/get_events.php", {
        offset : load_offset
    }, function(data){
        if(data == -1){
            html = "<div class='record'>" +
                "<h3>SERVER DOWN, REFRESH PAGE</h3>" +
                "</div> "
            $('#event-list').append(html);
        }else if(data == 0 ){
            $('.load').hide('slow');
        }else if(data == -2 ){
            html = "<div class='record'>" +
                "<h2>Invalid parameter sent</h2>" +
                "</div> "
            $('.event-list').html(html);
            $('.load').hide();
        }
        else{
            var events = Object.keys(data);
            events.forEach(function(record){
                html = "<li>" +
                    "<time datetime='"+ data[record].date.substr(0, 4) + "-" + data[record].date.substr(5, 2) + "-" + data[record].date.substr(8, 2) +"' >" +
                    "<span class='day'>" + data[record].date.substr(8, 2) + "</span> " +
                    "<span class='month'>" +  data[record].date.substr(5, 2)  + "</span> " +
                    "<span class='year'>" + data[record].date.substr(0, 4) + "</span> " +
                    "</time>" +
                    "<div class='info'> " +
                    "<h2 class='title'>" + capitaliseFirstLetter(data[record].event_name) + "</h2>" +
                    "<p class='desc'>" + data[record].event_description + "</p>" +
                    "<p class='desc'><span class='fa fa-location-arrow grey_text'></span>" + data[record].event_location + "</p>"
                    + "</div> " +
                    "</li>";
                $('.event-list').append(html);
            });
            load_offset += 5;
        }
    }, "json").fail(function(){
            console.log('fail');
        });
}

function capitaliseFirstLetter(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}