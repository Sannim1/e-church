/**
 * Created by caleb on 8/7/14.
 */

var geocoder;
var map;
var marker;
var data;

function initialize(init) {
    data = init;
    $('#locationModal').modal('show');
    geocoder = new google.maps.Geocoder();
    var latlngStr = init.split(",",2);
    var lat = parseFloat(latlngStr[0]);
    var lng = parseFloat(latlngStr[1]);
    var latlng = new google.maps.LatLng(lat, lng);
//    var latlng = new google.maps.LatLng(init);
    var mapOptions = {
        zoom: 8,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.HYBRID
    }
    setTimeout(function(){
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        placeMarker(latlng);
        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });
    }, 1000);
    codeLatLng(init);
}

function codeAddress() {
    var address = document.location.address.value;
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
            data = results[0].geometry.location.lat() + ", " + results[0].geometry.location.lng();
            console.log(results[0].geometry.location.lat());
            console.log(results[0].geometry.location.lng());
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
}

function placeMarker(location) {
    if(marker){ //check if the marker exists
        marker.setPosition(location); //on change of location
    }else{
        marker = new google.maps.Marker({
            position: location,
            map: map
        });
    }
    console.log(location.lat());
    console.log(location.lng());
//            getAddress(location);
}

function codeLatLng(init) {
    data = init;
    geocoder = new google.maps.Geocoder();
    var latlngStr = init.split(",",2);
    var lat = parseFloat(latlngStr[0]);
    var lng = parseFloat(latlngStr[1]);
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
//                console.log(results[1].formatted_address);
                document.getElementById('location').innerHTML = results[1].formatted_address;
                document.location.address.value = results[1].formatted_address;
            }
        } else {
            alert("Geocoder failed due to: " + status);
        }
    });
}

function submitLocation(){
    console.log(data);
    $.post("phase/map.php",
        {
            data: data
        },
        function(result){
            console.log(result);
            if(result == "shing"){
                document.getElementById('msg').innerHTML = 'Something went wrong';
            }
            else{
                document.getElementById('msg').innerHTML = 'Update Successful, Reloading web page now';
                setTimeout(location.reload(), 2000);
            }
        }
    );
}