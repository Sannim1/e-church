/**
 * Created with JetBrains PhpStorm.
 * User: user
 * Date: 9/4/14
 * Time: 6:25 PM
 * To change this template use File | Settings | File Templates.
 */
window.onload = init;
var delete_class;
function init(){
    var delete_class = document.getElementsByClassName("delete");
    var total_pdf = delete_class.length;
    // console.log(delete_class);
    for(var i = 0; i<total_pdf; i++){
        if(delete_class[i].addEventListener){
            delete_class[i].addEventListener("click", delete_media,  false)
        }
        else{
            //for older browsers
            delete_class[i].attachEvent("click", delete_media)
        }
    }

    //event listener for filt type onchange
    var file_type_sel = document.getElementById("m_type");
    var m_name = document.getElementById("m_name");
    m_name.placeholder = "Maximum filesize: 2MB";

    if(file_type_sel.addEventListener){
        file_type_sel.addEventListener("change", file_type_msg)
    }
    else{
        file_type_sel.attachEvent("change", file_type_msg)
    }

}

//manage pdf
function delete_media(){

    var verify =  confirm("Are you sure, you want to delete this media");
    if(verify){
        //    console.log(this.id);
        var med_id = this.id;
        var med_name = this.title;
        // console.log(id);
        $.post("manage_media.php",
            {
                media_id: med_id,
                media_name: med_name
            },
            function(result){
                if(result){
                    // alert("Record deleted successfully");
                    alert(result);
                }else{
                    alert("An unexpected error occur");
                }
                reloadPage();
            }
        );
    }
}

function file_type_msg(){
    //console.log(this.value);
    var m_name = document.getElementById("m_name");
    if(this.value == 0){
        m_name.placeholder = "Maximum filesize: 2MB";
    }
    else if(this.value == 1){
        m_name.placeholder = "Maximum filesize: 100MB";
    }
    else if(this.value == 2){
        m_name.placeholder = "Maximum filesize: 512MB";
    }
}

function showPicture(e){
    var url = e.getAttribute('data-url');
    var html = "<img style='width: 240px' src='"
        + url
        + "' />";
    var options = {
        "html" : true,
        "title" : "Picture preview",
        "content" : html,
        "placement" : "bottom"
    };
    $(e).popover(options);
}

function showAudio(e){
    var url = e.getAttribute('data-url');
    var html = "<audio style='width: 240px' controls preload='auto'><source src='"
        + url
        + "' type='audio/mpeg' /></audio>";
    var options = {
        "html" : true,
        "title" : "Playing Audio",
        "content" : html,
        "placement" : "bottom"
    };
    $(e).popover(options);
}

function showVideo(e){
    var url = e.getAttribute('data-url');
    var html = "<video width='100%' controls preload='auto' poster='images/banner.jpg'><source src='"
        + url
        + "' type='video/mp4' />"
        + "<object width='100%' type='application/x-shockwave-flash' data='"
        + url
        + "'> <!-- Firefox uses the `data` attribute above, IE/Safari uses the param below --> "
        + "<param name='movie' value='"
        + url
        + "' /> <param name='flashvars' value='controlbar=over&amp;file= " + url + "' /> "
        + "<!-- fallback image. note the title field below, put the title of the video there --> "
        + "<img src='images/banner.jpg' width='640' height='360'/></object></video>";
    console.log(html);
    $("#html").html(html);
//    var options = {
//        "html" : true,
//        "title" : "Playing Video",
//        "content" : html,
//        "placement" : "bottom"
//    };
//    $(e).popover(options);#
    $('#videoModal').modal('show');
}