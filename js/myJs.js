var bustcachevar=1; //bust potential caching of external pages after initial request? (1=yes, 0=no)
var bustcacheparameter="";

function createRequestObject(){
    try	{
        xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    }	catch(e)	{
        alert('Sorry, but your browser doesn\'t support XMLHttpRequest.');
    }
    return xmlhttp;
};

function ajaxpage(url, containerid){
    var page_request = createRequestObject();

    if (bustcachevar) bustcacheparameter=(url.indexOf("?")!=-1)? "&"+new Date().getTime() : "?"+new Date().getTime()
    page_request.open('GET', url+bustcacheparameter, true)
    page_request.send(null)

    page_request.onreadystatechange=function(){
        loadpage(page_request, containerid)
    }

}

function loadpage(page_request, containerid){
    if (page_request.readyState == 4 && (page_request.status==200 || window.location.href.indexOf("http")==-1)) {
        document.getElementById(containerid).innerHTML=page_request.responseText;
    };
}


//styling the input type file
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);

});
$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }

    });
});

function send(url){
    $("#mForm").on('submit',(function(e) {
        e.preventDefault();
        console.log(url);
        $.ajax({
            url: url,
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                $("#response").html(data);
            },
            error: function()
            {
                $("#response").html("<p>An error occurred</p>");
            }
        });
    }));
}

function uploadMediaForm(){
    if (document.uploadMedia.m_title.value !== '' && document.uploadMedia.m_desc.value !== '' && document.uploadMedia.m_name.value !== '') {
        var url = "phase/media.php";
        send(url);
    }
    else {
        document.getElementById('uploadError').innerHTML = 'Fill in all fields';
    }
}

function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function uploadNwLtrForm(){
    if (document.uploadMedia.m_title.value !== '' && document.uploadMedia.m_desc.value !== '' && document.uploadMedia.m_name.value !== '') {
        if(getExtension(document.uploadMedia.m_name.value) == "pdf"){
            url = "phase/phase_newsletter_upload.php";
            send(url);
        }
        else{
            document.getElementById('uploadError').innerHTML = 'File type should be pdf';
            return false;
        }
    } else {
        document.getElementById('uploadError').innerHTML = 'Fill in all fields';
    }
}

function uploadDevotionForm(){
    if (document.uploadMedia.m_title.value !== '' && document.uploadMedia.m_desc.value !== '' && document.uploadMedia.m_name.value !== '') {
        if(getExtension(document.uploadMedia.m_name.value) == "pdf"){
            url = "phase/phase_devotion_upload.php";
            send(url);
        }
        else{
            document.getElementById('uploadError').innerHTML = 'File type should be pdf';
            return false;
        }
    } else {
        document.getElementById('uploadError').innerHTML = 'Fill in all fields';
    }
}

function submitLoginForm(){
    $('.is-loading').removeClass('hidden');

    var email = document.getElementById('ch_email').value;
    var pass = document.getElementById('password').value;
    var location = "phase/signin.php";

    if (email !== '' && pass  !== '') {
        console.log(email);
        $.post(location,
            {
                email: email,
                pass: pass
            },
            function(result){
                console.log(result);
             process_loader(result, 'sign-error', 'sign-success', false);

//                if(result == -1){
//                   process_loader(result, 'sign-error', 'sign-success', false);
//                    document.getElementById('error').innerHTML = 'Incorrect Login Details';
//                }
//                else if(result == 1){
//                    document.getElementById('error').innerHTML = '';
//                    document.getElementById('success').innerHTML = 'Login Successful, Redirecting you now';
//                    setTimeout(function(){window.location.assign('profile.php')}, 2000);
//                }else{
//                    document.getElementById('error').innerHTML = 'An unexpected error occur';
//                }
            }
        );
    } else {
        document.getElementById('error').innerHTML = 'Fill in all fields';
    }
    return false;
}


function updateChurchProfile(){
    //show the loader
    $('.is-loading').removeClass('hidden');

    var name = document.profile.name.value;
    var vision = document.profile.vision.value;
    var mission = document.profile.mission.value;
    var description = document.profile.description.value;

    if (name !== '' && description  !== '') {
        $.post("phase/edit_profile.php",
            {
                name: name,
                vision: vision,
                mission: mission,
                description: description
            },
            function(result){
                process_loader(result, 'profile-error', 'profile-success', 'profileModal')
            }
        );
    } else {
        document.getElementById('msg').innerHTML = 'Fill in all fields';
    }
}

function updatePastorProfile(){
    //show the loader
    $('.is-loading').removeClass('hidden');

    var name = document.pastor.name.value;
    var number = document.pastor.number.value;
    var email = document.pastor.email.value;

    if (name !== '' && email  !== '') {
        $.post("phase/edit_pastor_profile.php",
            {
                name: name,
                number: number,
                email: email
            },
            function(result){
                process_loader(result, 'pst_error', 'pst_success', 'pastorModal');
            }
        );
    } else {
        document.getElementById('error').innerHTML = 'Fill in all fields';
    }
}

function process_loader(result, error_id, success_id, modal_id){
    if(result == -1){
        setTimeout(function(){
            document.getElementById(error_id).innerHTML = 'Operation not successful';
            $('.is-loading').addClass('hidden');
        }, 1000);

    }
    else if(result == 1){
        setTimeout(function(){
            document.getElementById(error_id).innerHTML = '';
            document.getElementById(success_id).innerHTML = 'Operation Successful';
            //hide the loader
            $('.is-loading').addClass('hidden');
            if(modal_id){
                setTimeout(function(){
                    $('#'+modal_id).modal('hide');
                    window.reloadPage();
                }, 1500);
            }else{
                setTimeout(function(){
                    window.reloadPage();
                }, 500)
            }
        }, 1000) ;
    }else{
        console.log("unexpected issue occur");
    }
}

function changePassword(){
    //show the loader
    $('.is-loading').removeClass('hidden');
    var username = document.admin.username.value;
    var pass = document.admin.password.value;

    if (username !== '' && pass  !== '') {
        $.post("phase/change_password.php",
            {
                username: username,
                pass: pass
            },
            function(result){
                process_loader(result, 'chg_psw_error', 'chg_psw_success', 'passwordModal');
//                console.log(result);
//                if(result == "shing"){
//                    document.getElementById('dialog').innerHTML = 'Something went wrong';
//                }
//                else{
//                    document.getElementById('dialog').innerHTML = 'Update Successful';
//                    setTimeout(function(){
//                        $('#passwordModal').modal('hide');
//                    }, 1500);
//                }
            }
        );
    } else {
        document.getElementById('dialog').innerHTML = 'Fill in all fields';
    }
}

function reloadPage(){
    window.location.reload();
}

function gcm(){
    var message = document.getElementById('message').value;
    var title = document.getElementById('title').value;
    //console.log(title.length);
    if (message == '' && title == '') {
        document.getElementById('msg').innerHTML = 'Fill in all fields';

    }else if(title.length > 50){
        document.getElementById('msg').innerHTML = 'Title of message should not be more than 50 characters';

    }else{
        $.post("phase/phase_gcm_notification.php",
            {
                message: message,
                title : title
            },
            function(data){
                console.log(data);
                var result = JSON.parse(data);
                if(result.status == 1){
                    document.getElementById('msg').innerHTML = 'Message Posting Successful';
                    console.log('sending');
                    setTimeout(function(){
                        reloadPage();
                    }, 2000);
                }
                else{
                    document.getElementById('msg').innerHTML = 'Something went wrong';
                }
            }
        );
    }
}

function postEvent(){
    var name = document.getElementById('name').value;
    var desc = document.getElementById('desc').value;
    var location = document.getElementById('location').value;
    var date = document.getElementById('date').value;
    var time = document.getElementById('time').value;

    if (name !== '' && date != "") {
        document.getElementById('msg').innerHTML = 'Scheduling your event, Please be patient';
        setTimeout(function(){
            window.location = "phase/phase_calendar.php?name=" + name
                + "&location=" + location + "&date=" + date
                + "&time=" + time + "&desc=" + desc;
        }, 1500);
    } else {
        document.getElementById('msg').innerHTML = 'Fill in all fields correctly';
    }
}

function deleteNewsltr(id, file_name){
    var verify =  confirm("Are you sure, you want to delete");
    if(verify){
        ajaxpage("core/manage_inspiration.php?do=deleteInsp&file_id=" + id +"&file_name=" + file_name, "content");

    }

}function deleteDevo(id, file_name){
    var verify =  confirm("Are you sure, you want to delete");
    if(verify){
        ajaxpage("core/manage_inspiration.php?do=deleteDevo&file_id=" + id +"&file_name=" + file_name, "content");

    }

}

