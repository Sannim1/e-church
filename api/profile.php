<?php
/**
 * Created by PhpStorm.
 * User: caleb
 * Date: 9/12/14
 * Time: 6:59 PM
 */

require_once('data_access/DataTable.php');
require_once('data_access/SqlStatement.php');
require_once('data_access/SqlClient.php');

require_once('core/base.php');
require_once('core/Profile.php');

require_once 'utils/JsonResponse.php';

//$params = array("file_type"=>0);
//$test =  new Media($params);
//var_dump($test->getMedia());

if($_SERVER["REQUEST_METHOD"] == "GET"){
    $records = new Profile();
    $output = $records->getProfile();
    if(!empty($output)){
        echo JsonResponse::message(JsonResponse::STATUS_OK, $output);
    }
    else{
        echo JsonResponse::message(JsonResponse::STATUS_OK, "No record");
    }

    exit();

    /*
    //check if file type is specified
    if(isset($_GET["auth_token"])){
        $file_type = $_GET["auth_token"];
        $records = new Media($file_type);
        if($records->checkType()){
            $output = $records->getMedia();
            if(!empty($output)){
                echo JsonResponse::message(JsonResponse::STATUS_OK, $output);
            }
            else{
                echo JsonResponse::message(JsonResponse::STATUS_OK, "No record");
            }

            exit();
        }
        else{
            echo JsonResponse::error("Invalid media type");
            exit();
        }
    }
    else{
        echo JsonResponse::error("Media type not specified");
        exit();
    }
    */
}
else{
    echo JsonResponse::error("Invalid access");
    exit();
}