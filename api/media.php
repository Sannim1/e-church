<?php
/**
 * Created by JetBrains PhpStorm.
 * User: User
 * Date: 9/11/14
 * Time: 12:51 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('data_access/DataTable.php');
require_once('data_access/SqlStatement.php');
require_once('data_access/SqlClient.php');

require_once('core/base.php');
require_once('core/Media.php');

require_once 'utils/JsonResponse.php';
require_once 'utils/Utilities.php';

//$params = array("file_type"=>0);
//$test =  new Media($params);
//var_dump($test->getMedia());

if($_SERVER["REQUEST_METHOD"] == "GET"){
    //check if file type is specified
    if(isset($_GET["file_type"])){
        $file_type = $_GET["file_type"];
        $records = new Media($file_type);
        if($records->checkType()){
            $output = $records->getMedia();
            if(!empty($output)){
                echo JsonResponse::message(JsonResponse::STATUS_OK, $output);
            }
            else{
                echo JsonResponse::message(JsonResponse::STATUS_OK, "No record");
            }

            exit();
        }
        else{
            echo JsonResponse::error("Invalid media type");
            exit();
        }
    }
    else{
        echo JsonResponse::error("Media type not specified");
        exit();
    }
}
else{
    echo JsonResponse::error("Invalid access");
    exit();
}
