<?php


class Media extends base{
    var $file_description;
    var $file_name;
    var $file_type;
    var $file_title;

    public function __construct($media_type){
       $this->setFileType($media_type);
    }

    public function setFileDescription($file_description)
    {
        $this->file_description = $file_description;
    }

    public function getFileDescription()
    {
        return $this->file_description;
    }

    public function setFileName($file_name)
    {
        $this->file_name = $file_name;
    }

    public function getFileName()
    {
        return $this->file_name;
    }

    public function setFileTitle($file_title)
    {
        $this->file_title = $file_title;
    }

    public function getFileTitle()
    {
        return $this->file_title;
    }

    public function setFileType($file_type)
    {

            $this->file_type = $file_type;

    }

    public function getFileType()
    {
        return $this->file_type;
    }

    public function checkType(){
        return ($this->file_type == 0 || $this->file_type == 1 || $this->file_type == 2) ? TRUE : FALSE;
    }
    public function getMedia(){
        $statement = MediaSqlStatement::GET;
        $param = array("file_type"=>$this->file_type);
        $records = $this->executeQuery($statement, $param);
        if(!empty($records)){
            for($i = 0; $i < sizeof($records); $i++)
                $records[$i]['file_name'] = Utilities::getBaseURL() . $records[$i]['file_name'];
        }
        return $records;
    }


}

