<?php

/**
 * Core Base Class
 */
abstract class Base{

    /*Used for checking the status of the database response*/
    protected $db_status;

    /**
     * Constructor
     *
     * @param null $id
     */
    public function __construct($id = NULL){

    }

    public function getAsArray(){
        return array();
    }

    public function executeQuery($stmt, $data, $fetch_row = false){
        $params = $this->parameterize($data);
        $sql_client = new SqlClient();
        if ($sql_client->getStatus()){
            $pds = $sql_client->executeQuery($stmt, $params);
            return ($fetch_row) ? $sql_client->fetchRow($pds) : $sql_client->fetchAll($pds);
        }
        return NULL;
    }

    public function executeNonQuery($stmt, $data){
        $params = $this->parameterize($data);
        $sql_client = new SqlClient();
        if ($sql_client->getStatus()){
            return $sql_client->executeNonQuery($stmt, $params);
        }
        return NULL;
    }

    /**
     * It gets the status of the previous database request
     *
     * @return mixed
     */
    public function getDbStatus(){
        return $this->db_status;
    }

    protected function parameterize($array){
        $params = array();
        $keys = array_keys($array);
        foreach($keys as $key){
            $params[':'.$key] = $array[$key];
        }
        return $params;
    }

    public function strReplace($old_text, $new_text, $string){
        return str_replace('@'.$old_text, $new_text, $string);
    }
}
?>