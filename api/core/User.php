<?php
  class User extends Base{

    private $email = NULL;
    private $auth_token = NULL;
    private $gcm_id = NULL;
    private $device_id = NULL;

    public function __construct($userdata){
      if (isset($userdata["email"])) {
        # code...
        $this->email = $userdata["email"];
      }
      if (isset($userdata["auth_token"])) {
        # code...
        $this->auth_token = $userdata["auth_token"];
      }
      if (isset($userdata["gcm_id"])) {
        $this->gcm_id = $userdata["gcm_id"];
      }
      if (isset($userdata["device_id"])) {
        $this->device_id = $userdata["device_id"];
      }
    }

    public function addUser() {
      if ($this->userExists()) {
        return $this->updateUser();
      }
      $statement = AppUserSqlStatement::ADD;
      $data = array();
      $data[AppUserTable::email] = $this->email;
      $data[AppUserTable::device_id] = $this->device_id;
      $data[AppUserTable::auth_token] = $this->auth_token;
      $data[AppUserTable::gcm_id] = $this->gcm_id;

      $result = $this->executeNonQuery($statement, $data);
      return ($result != -1 and $result != 0) ? true : false;
    }

    public function verifyUser() {
      $statement = AppUserSqlStatement::VERIFY_USER;
      $data = array();
      $data[AppUserTable::email] = $this->email;
      $data[AppUserTable::auth_token] = $this->auth_token;
      $result = $this->executeQuery($statement, $data);
      return (sizeof($result) == 1) ? true : false;
    }

    public function updateUser() {
      if ($this->email === NULL || $this->auth_token === NULL || $this->gcm_id === NULL || $this->device_id === NULL) {
        return false;
      }

      $statement = AppUserSqlStatement::UPDATE_USER;
      $data = array();
      $data[AppUserTable::email] = $this->email;
      $data[AppUserTable::device_id] = $this->device_id;
      $data[AppUserTable::auth_token] = $this->auth_token;
      $data[AppUserTable::gcm_id] = $this->gcm_id;
      $result = $this->executeNonQuery($statement, $data);
      return ($result != -1 and $result != 0) ? true : false;
    }

    private function userExists() {
      $statement = AppUserSqlStatement::CHECK_USER;
      $data = array();
      $data[AppUserTable::email] = $this->email;
      $data[AppUserTable::device_id] = $this->device_id;
      $result = $this->executeQuery($statement, $data);
      return $result[0]['count'] > 0;
    }
  }
?>