<?php

class Profile extends base{

    var $pastorName;
    var $pastorEmail;
    var $churchEmail;
    var $vision;
    var $mission;
    var $description;
    var $churchName;
    var $churchLocation;

    function __construct(){

    }

    /**
     * @param mixed $churchEmail
     */
    public function setChurchEmail($churchEmail)
    {
        $this->churchEmail = $churchEmail;
    }

    /**
     * @return mixed
     */
    public function getChurchEmail()
    {
        return $this->churchEmail;
    }

    /**
     * @param mixed $churchLocation
     */
    public function setChurchLocation($churchLocation)
    {
        $this->churchLocation = $churchLocation;
    }

    /**
     * @return mixed
     */
    public function getChurchLocation()
    {
        return $this->churchLocation;
    }

    /**
     * @param mixed $churchName
     */
    public function setChurchName($churchName)
    {
        $this->churchName = $churchName;
    }

    /**
     * @return mixed
     */
    public function getChurchName()
    {
        return $this->churchName;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $mission
     */
    public function setMission($mission)
    {
        $this->mission = $mission;
    }

    /**
     * @return mixed
     */
    public function getMission()
    {
        return $this->mission;
    }

    /**
     * @param mixed $pastorEmail
     */
    public function setPastorEmail($pastorEmail)
    {
        $this->pastorEmail = $pastorEmail;
    }

    /**
     * @return mixed
     */
    public function getPastorEmail()
    {
        return $this->pastorEmail;
    }

    /**
     * @param mixed $pastorName
     */
    public function setPastorName($pastorName)
    {
        $this->pastorName = $pastorName;
    }

    /**
     * @return mixed
     */
    public function getPastorName()
    {
        return $this->pastorName;
    }

    /**
     * @param mixed $vision
     */
    public function setVision($vision)
    {
        $this->vision = $vision;
    }

    /**
     * @return mixed
     */
    public function getVision()
    {
        return $this->vision;
    }

    public function getProfile(){
        $statement = ProfileSqlStatement::GET;
        $param = array();
        $records = $this->executeQuery($statement, $param);
        return $records[0];
    }
}

