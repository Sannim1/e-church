<?php
  class ContentRequest extends Base {
    public static function getDevotionals(){
      $reqObject = new ContentRequest();
      $statement = DevotionsSqlStatement::GET;
      $data = array();
      $result = $reqObject->executeQuery($statement, $data);
      return $result;
    }

    public static function getNewsletters(){
      $reqObject = new ContentRequest();
      $statement = NewslettersSqlStatement::GET;
      $data = array();
      $result = $reqObject->executeQuery($statement, $data);
      return $result;
    }

    public static function generateURL($file_type, $file_name) {
      $baseurl = Utilities::getBaseURL();
      $file_name = trim($file_name);
      return trim($baseurl . $file_type . "/" . $file_name);
    }

    public static function processResponse($file_type, $raw) {
      $result = array();
      foreach ($raw as $processing) {
        $processing["url"] = ContentRequest::generateURL($file_type, $processing["file_name"]);
        unset($processing["file_name"]);
        array_push($result, $processing);
      }

      return $result;
    }
  }
?>