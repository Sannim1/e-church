<?php

class Blog extends base{
    var $simpleXML;

    function __construct(){
        $file = file_get_contents(BLOGURL);
        $this->simpleXML = new SimpleXMLElement($file);
    }

    public function fetch(){
        return array($this->simpleXML->channel->item);
    }
}

