-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 09, 2014 at 06:15 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `echurch`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_user`
--

CREATE TABLE IF NOT EXISTS `app_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `auth_token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `app_user`
--

INSERT INTO `app_user` (`id`, `email`, `auth_token`) VALUES
(1, 'san@gmail.com', '8ak9lky6de'),
(2, 'kesan@gmail.com', 'k9a');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `church_id` int(5) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `file_type` int(1) NOT NULL,
  `file_title` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `news_dev`
--

CREATE TABLE IF NOT EXISTS `news_dev` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_title` varchar(128) NOT NULL,
  `file_description` varchar(128) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `file_type` int(1) NOT NULL,
  `church_id` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `message`, `time`) VALUES
(1, 'test', 1407589274),
(2, 'test', 1407592889),
(3, '', 1407593577),
(4, '', 1407593875),
(5, 'testing', 1407593895),
(6, 'testing', 1407594199),
(7, 'testing', 1407594208),
(8, 'testing', 1407594647),
(9, 'testing', 1407594770);

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `church_id` int(5) NOT NULL,
  `status` int(1) NOT NULL,
  `um_feat` int(1) NOT NULL,
  `bs_feat` int(1) NOT NULL,
  `dnl_feat` int(1) NOT NULL,
  `s_feat` int(1) NOT NULL,
  `mm_feat` int(1) NOT NULL,
  `sp_feat` int(1) NOT NULL,
  `og_feat` int(1) NOT NULL,
  `mp_feat` int(1) NOT NULL,
  `em_feat` int(1) NOT NULL,
  `cp_feat` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE IF NOT EXISTS `signup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `church_name` varchar(128) NOT NULL,
  `church_location` varchar(128) NOT NULL,
  `pastor_name` varchar(128) NOT NULL,
  `pastor_number` varchar(128) NOT NULL,
  `pastor_email` varchar(128) NOT NULL,
  `church_email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `vision` varchar(500) NOT NULL,
  `mission` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `um_feat` int(1) NOT NULL,
  `bs_feat` int(1) NOT NULL,
  `dnl_feat` int(1) NOT NULL,
  `s_feat` int(1) NOT NULL,
  `mm_feat` int(1) NOT NULL,
  `sp_feat` int(1) NOT NULL,
  `og_feat` int(1) NOT NULL,
  `mp_feat` int(1) NOT NULL,
  `em_feat` int(1) NOT NULL,
  `cp_feat` int(1) NOT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`id`, `church_name`, `church_location`, `pastor_name`, `pastor_number`, `pastor_email`, `church_email`, `password`, `vision`, `mission`, `description`, `um_feat`, `bs_feat`, `dnl_feat`, `s_feat`, `mm_feat`, `sp_feat`, `og_feat`, `mp_feat`, `em_feat`, `cp_feat`, `status`) VALUES
(1, 'Foursquare', '7.517722099999999 , 4.5263479999999845', 'Skelewu', '080419911419', 'theif@ole.com', 'testing@echurch.com', 'password', 'Testing', 'Testing', 'Testing still Testing', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, b'0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
