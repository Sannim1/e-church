<?php
  class AppUserTable {
    const id = 'id';
    const email = 'email';
    const device_id = 'device_id';
    const auth_token = 'auth_token';
    const gcm_id = 'gcm_id';
    const create_date = 'create_date';
    const active_fg = 'active_fg';
    const modified_date = 'modified_date';
  }

  class DevotionsTable {
    const id = 'id';
    const file_title = 'file_title';
    const file_description = 'file_description';
    const file_name = 'file_name';
    const created_date = 'created_date';
    const modified_date = 'modified_date';
  }

  class NewslettersTable {
    const id = 'id';
    const file_title = 'file_title';
    const file_description = 'file_description';
    const file_name = 'file_name';
    const created_date = 'created_date';
    const modified_date = 'modified_date';
  }

class MediaTable{
    const id ='id';
    const file_description = 'file_description';
    const file_name = 'file_name';
    const file_type = 'file_type';
    const file_title = 'file_title';
    const created_date = 'created_date';
    const modified_date = 'modified_date';
}

class SignUpTable{

    const table_name = "signup";

    const church_name = "church_name";
    const church_location = "church_location";
    const pastor_name = "pastor_name";
    const pastor_number = "pastor_number";
    const pastor_email = "pastor_email";
    const church_email = "church_email";
    const password = "password";
    const vision = "vision";
    const mission = "mission";
    const description = "description";
    const um_feat = "um_feat";
    const bs_feat = "bs_feat";
    const dnl_feat = "dnl_feat";
    const s_feat = "s_feat";
    const mm_feat = "mm_feat";
    const sp_feat = "sp_feat";
    const og_feat = "og_feat";
    const mp_feat = "mp_feat";
    const em_feat = "em_feat";
    const cp_feat = "cp_feat";
    const status = "status";
    const created_date = 'created_date';
    const modified_date = 'modified_date';
}
?>