<?php
  class AppUserSqlStatement{
    const ADD = "INSERT INTO app_user(email, device_id, auth_token, gcm_id, create_date, active_fg, modified_date) VALUES(:email, :device_id, :auth_token, :gcm_id, NOW(), 1, NOW())";
    const VERIFY_USER = "SELECT * FROM app_user WHERE email = :email AND auth_token = :auth_token";
    const UPDATE_USER = "UPDATE app_user SET auth_token = :auth_token, device_id = :device_id, gcm_id = :gcm_id, modified_date = NOW() WHERE email = :email";
    const CHECK_USER = "SELECT COUNT(*) AS count FROM app_user WHERE email = :email AND device_id = :device_id";
  }

  class DevotionsSqlStatement {
    const GET = "SELECT * FROM devotions";
  }

  class NewslettersSqlStatement {
    const GET = "SELECT * FROM newsletters";
  }
class MediaSqlStatement{
    const  GET = "SELECT * FROM media WHERE file_type = :file_type";
}

class ProfileSqlStatement{
    const  GET = "SELECT * FROM signup";
}
?>