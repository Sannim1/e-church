<?php
  require_once 'data_access/DataTable.php';
  require_once 'data_access/SqlStatement.php';
  require_once 'data_access/SqlClient.php';

  require_once 'core/base.php';
  require_once 'core/User.php';
  require_once 'core/Profile.php';

  require_once 'utils/JsonResponse.php';

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    # code...
    if (isset($_POST["intent"])) {
      $intent = $_POST["intent"];
    } else {
      echo JsonResponse::error("No intent set for this request!");
      exit();
    }

    if (isset($_POST["email"], $_POST["auth_token"], $_POST["gcm_id"])) {
      $userdata = array();
      $userdata["email"] = $_POST["email"];
      $userdata["auth_token"] = $_POST["auth_token"];
      $userdata["gcm_id"] = $_POST["gcm_id"];
      $userdata["device_id"] = $_POST["device_id"];

      $user = new User($userdata);
    } else {
      echo JsonResponse::error("Incomplete request parameters!");
      exit();
    }


    if ($intent == "newUser") {
      $output = $user->addUser();
      if ($output) {
        $records = new Profile();
        $profile = $records->getProfile();
        echo JsonResponse::message(JsonResponse::STATUS_OK, $profile);
        exit();
      } else {
        echo JsonResponse::error("Unable to add new user!");
        exit();
      }
    } elseif ($intent == "validate") {
      $output = $user->verifyUser();
      if ($output) {
        exit();
      } else {
        echo JsonResponse::error("Invalid combination of user credentials. Please sign in again!");
        exit();
      }
    } elseif ($intent == "update") {
      $output = $user->updateUser();
      if ($output) {
        # code...
        echo JsonResponse::message(JsonResponse::STATUS_OK, "User update succesful!");
        exit();
      } else {
        echo JsonResponse::error("Unable to update user credentials!");
        exit();
      }
    } else {
      echo JsonResponse::error("Request not recognized!");
      exit();
    }
  } else {
    echo JsonResponse::error("Invalid request method!");
    exit();
  }
?>