<?php
  require_once 'data_access/Constants.php';
  require_once 'data_access/DataTable.php';
  require_once 'data_access/SqlStatement.php';
  require_once 'data_access/SqlClient.php';

  require_once 'core/base.php';
  require_once 'core/ContentRequest.php';

  require_once 'utils/JsonResponse.php';
  require_once 'utils/Utilities.php';

  if ($_SERVER["REQUEST_METHOD"] == "GET") {
    if (isset($_REQUEST["intent"])) {
      $intent = $_REQUEST["intent"];
    } else {
      echo JsonResponse::error("No intent set for this request");
      exit();
    }

    if ($intent == "getAllDevotionals") {
      $_result = ContentRequest::getDevotionals();
      $result = ContentRequest::processResponse(DEVOTIONS, $_result);
      echo JsonResponse::success($result);
      exit();
    } elseif ($intent == "getAllNewsletters") {
      $_result = ContentRequest::getNewsletters();
      $result = ContentRequest::processResponse(NEWSLETTERS, $_result);
      echo JsonResponse::success($result);
      exit();
    } else {
      echo JsonResponse::error("Invalid intent!");
      exit();
    }
  } else {
    echo JsonResponse::error("Invalid request method!");
    exit();
  }
?>