<?php
/**
 * Created by PhpStorm.
 * User: caleb
 * Date: 9/12/14
 * Time: 6:59 PM
 */

require_once('data_access/Constants.php');
//var_dump(BLOGURL);
//exit();

require_once('core/base.php');
require_once('core/Blog.php');

require_once('utils/JsonResponse.php');

if($_SERVER["REQUEST_METHOD"] == "GET"){
    $data = new Blog();
    $output = $data->fetch();

    if(!empty($output)){
        echo JsonResponse::message(JsonResponse::STATUS_OK, $output);
    }
    else{
        echo JsonResponse::message(JsonResponse::STATUS_OK, "No record");
    }

    exit();
}
else{
    echo JsonResponse::error("Invalid access");
    exit();
}