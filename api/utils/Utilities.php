<?php
  class Utilities {
    public static function dump($object){
      var_dump($object);
    }

    public static function setPublicIP(){
      return file_get_contents("http://icanhazip.com");
    }

    public static function getPublicIP(){
      $directory = getcwd();
      //return $directory . "/utils/publicIP.txt";
      return file_get_contents($directory . "/utils/publicIP.txt");
    }

    public static function getBaseURL() {
      $baseurl = "http://" . trim(Utilities::getPublicIP()) . "/";
      return $baseurl;
    }
  }
?>