<?php
    session_start();
  if(isset($_SESSION['user'])){
      header("Location:profile.php");
  }
?>

<!DOCTYPE HTML>
<html>
<head>
    <title>eCHURCH App</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/mystyle.css">
    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.dropotron.min.js"></script>
    <script src="js/skel.min.js"></script>
<!--    <script src="js/skel-layers.min.js"></script>-->
    <script src="js/init.js"></script>
    <script src="js/myJs.js"></script>
    <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />

    </noscript>
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
</head>

<body>

<!-- Header -->
<!--<div id="header">-->
<!--</div>-->

<!-- Banner -->
<section class="wrapper style2">
    <div class="container">
        <header class="major">
            <h2>E-CHURCH <em>App</em></h2>
            <p>
                A Cross-Platform Mobile Application Management System for Churches
            </p>
        </header>
    </div>
</section>

<!-- Highlights -->
<section>
    <div  class="fill">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="modal-title" id="signUpModalLabel">Church ADMIN Login</h4>
            </div>
            <div class="panel-body">
                <div id="response">
                    <form onsubmit="submitLoginForm(); return false;">
                        <div class="is-loading hidden">
                            <img src=" images/loading.gif">
                        </div>
                        <div class="text-success text-center" id="sign-success"></div>
                        <div class="text-danger text-center" id="sign-error"></div>
                        <input class="form-control ejo" placeholder="Church Email Address" id="ch_email" type="email">
                        <input class="form-control ejo" placeholder="Password" id="password" type="password">

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Sign in</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>


<!-- Footer -->
<div id="footer">
    <!-- Icons -->
    <ul class="icons">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa-github"><span class="label">GitHub</span></a></li>
        <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
    </ul>

    <!-- Copyright -->
    <div class="copyright">
        <ul class="menu">
            <li>&copy; eCHURCH. All rights reserved</li>
        </ul>
    </div>

</div>
<script src="js/bootstrap.js"></script>
</body>
</html>